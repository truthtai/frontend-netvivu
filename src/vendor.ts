// For vendors for example jQuery, Lodash, angular2-jwt just import them here unless you plan on
// chunking vendors files for async loading. You would need to import the async loaded vendors
// at the entry point of the async loaded file. Also see custom-typings.d.ts as you also need to
// run `typings install x` where `x` is your module

// Angular 2
import 'angular2/platform/browser';
import 'angular2/core';
import 'angular2/http';
import 'angular2/router';
// import 'zone.js/dist/zone.min.js';
// RxJS
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';

import 'jquery';
import 'bootstrap-loader';
import 'rxjs/Rx';
import 'font-awesome-webpack';
import 'lodash';
import 'angular2-jwt';
// import 'ng2-notifications';
// import 'ckeditor';
// import 'ng2-ckeditor';
// import 'angular2-toaster';
import 'sweetalert';
import 'toastr';
// import 'socket.io-client';
// import 'tinymce';
// import 'eonasdan-bootstrap-datetimepicker';

if ('production' === ENV) {
  // Production


} else {
  // Development

}
