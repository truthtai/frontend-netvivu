import {Component} from 'angular2/core';

/*
 * We're loading this component asynchronously
 * We are using some magic with es6-promise-loader that will wrap the module with a Promise
 * see https://github.com/gdi2290/es6-promise-loader for more info
 */

@Component({
  selector: 'footer-component',
  template: require('./footer.html'),
  styles: [require('./footer.scss')]
})
export class FooterComponent {
  constructor() {

  }

  ngOnInit() {
    console.log('hello `header` component');
  }

}
