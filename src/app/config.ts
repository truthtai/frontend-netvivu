// declare var ENV:any;
let API_URL;
if (ENV === 'development') {
  API_URL = 'http://localhost:3333';
} else {
  API_URL = 'http://api.giaidaugame.com';
}

export const CONFIG = {
  API_URL
};
