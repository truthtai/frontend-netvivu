import {Component} from 'angular2/core';
import {NavHeader} from '../navHeader/navHeader';
import {SliderComponent} from '../slider/slider';


@Component({
  selector: 'not-logged',
  template: require('./notLogged.html'),
  styles: [require('./notLogged.scss')],
  directives: [NavHeader, SliderComponent]
})

export class NotLogged {
  news: any;
  constructor() {
  }

  ngOnInit() {
    // console.log('hello `header` component');
  }

}
