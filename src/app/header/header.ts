import {Component, Output, EventEmitter} from 'angular2/core';
import {FormBuilder, Validators} from 'angular2/common';
import {Router} from 'angular2/router';
import {UserService} from '../shared/services/user';
import { ValidationService } from '../shared/services/validation';
import {JwtHelper} from 'angular2-jwt';
import { SweetAlertService } from '../shared/services/sweet-alert';
import { AddressService } from '../shared/services/address';
import {DateFormatPipe} from 'angular2-moment';
import {SocketService} from '../shared/services/socket';
import {MailboxService} from '../shared/services/mailbox';

declare var window: any;
type IMailbox = HeaderComponent[];
@Component({
  selector: 'header-component',
  template: require('./header.html'),
  styles: [require('./header.scss')],
  pipes: [DateFormatPipe],
  providers: [
    UserService,
    SweetAlertService,
    SocketService,
  // ValidationService,
    MailboxService,
    AddressService,
    JwtHelper],
  // directives: []
})

export class HeaderComponent {
  loginForm: any;
  registerForm: any;
  upgradeCPMForm: any;

  infoReg: any;
  info: any;

  userData: any;
  cities: any;
  districts: any;

  isRegSuccess: boolean; isRegError: boolean; messageReg: string;
  isLoginSuccess: boolean; isLoginError: boolean; messageLogin: string;
  isRequestedPassword = false;
  inboxUnread: any = [];

  submitting = false;

  xu = 200;
  @Output() userDataEvt = new EventEmitter();
  constructor(
    private _fb: FormBuilder,
    public _user: UserService,
    public _jwt: JwtHelper,
    public router: Router,
    public _notif: SweetAlertService,
    public _address: AddressService,
    public _SS: SocketService,
    public _MB: MailboxService
    ) {

    this._address.getAllCity().subscribe(res => this.cities = res, err => console.log(err));
    this.loginForm = this._fb.group({
      'username': ['', Validators.required],
      'password': ['', Validators.compose(
        [Validators.required, ValidationService.passwordValidator
        ]
        )],
    });

    this.registerForm = this._fb.group({
      'username': ['', Validators.required],
      'password': ['', Validators.compose(
        [
          Validators.required,
          ValidationService.passwordValidator
        ])],
      'email': ['', Validators.compose([Validators.required, ValidationService.emailValidator])],
      'phone': ['', Validators.required],
      'name': ['', Validators.required],
    });

    this.upgradeCPMForm = this._fb.group({
      'businessName': ['', Validators.required],
      'address': ['', Validators.required],
      'bank': ['', Validators.required],
      'gpkd': ['', Validators.required],
      'district': ['', Validators.required],
      'city': ['', Validators.required],
    });

    this.userData = this._user.extract();
    this._SS.getInfo().subscribe(
      res => {
        this.userData = res;
      }
      , err => { console.log(err); }
      );

    this.getInbox();
    this.getInbox$();
  }

  ngOnInit() {
    // var token = localStorage.getItem('auth_token');
    // if (token) {
    //   this._user.getToken().subscribe((res: any) => {
    //   }, err => console.log('err'));
    //   this._SS.getInfo().subscribe(data => this.userData = data);
    // }

    // var stickyOffset = (<any>$('header-component')).offset().top;
    // (<any>$(window)).scroll(function() {
    //   let sticky: any = $('.nav-menu');
    //   let scroll = (<any>$(window)).scrollTop();
    //   if (scroll > stickyOffset) {
    //     sticky.addClass('fixed');
    //   } else {
    //     sticky.removeClass('fixed');
    //   }
    // });
  }

  getInbox$() {
    this._SS.getInbox()
      .subscribe((res: IMailbox[]) => {
      this.inboxUnread = res.filter((mail: any) => {
        return mail.receiver.action.read === false;
      });
    }, (error) => { });
  }

  getInbox() {
    this._MB.getInbox()
      .subscribe((res: IMailbox[]) => {
      this.inboxUnread = res.filter((mail: any) => {
        return mail.receiver.action.read === false;
      });
    }, (error) => { });
  }

  _extractToken(res: any) {
    this.userData = this._jwt.decodeToken(res);
    localStorage.setItem('auth_token', res);
  }

  doLogin() {

    if (this.loginForm.dirty && this.loginForm.valid) {

      this._user.login(this.loginForm.value)
        .subscribe(
        res => {
          this.userData = this._user._add_token_key(res);
          (<any>$('#login')).modal('hide');
          location.reload();
          // this._notif.swal('Bạn đã đăng nhập thành công. Chúc bạn một ngày vui vẻ!', 'success');
        },
        error => {
          this.isLoginError = true;
          this.messageLogin = <any>error.message;
          this._notif.swal('Thông tin đăng nhập sai', 'error');
        });

    }
  }

  doRegister() {

    if (this.registerForm.dirty && this.registerForm.valid) {
      this.infoReg = {
        'username': this.registerForm.value.username,
        'email': this.registerForm.value.email,
        'phone': this.registerForm.value.phone,
        'password': this.registerForm.value.password,
        'name': this.registerForm.value.name
      };
      // console.log(this.infoReg);
      if (this.userData && this.userData.type === 'CPM') { this.infoReg.isCPM = true; };
      this._user.register(this.infoReg)
        .subscribe(
        res => {
          if (!this.userData) {
            this.userData = this._user._add_token_key(res);

          } else {
            this._notif.swal('Bạn đã đăng ký thành công. Chúc bạn một ngày vui vẻ!', 'success');
          }
          (<any>$('#register')).modal('hide');
          location.reload();

        },
        error => { this.isRegError = true; this.messageReg = <any>error.message; });

    }
  }

  doLogout() {
    localStorage.removeItem('auth_token');
    this.userData = '';
    this.router.navigateByUrl('/home');
    location.reload();
  }

  upgradeCPM() {
    if (this.userData.credit >= this.xu) {
      (<any>$('#upgradeCPM')).modal('show');
    } else {
      //alert('Bạn không đủ xu để nâng cấp. Vui lòng nạp thêm xu');
      this._notif.swal('Bạn không đủ xu để nâng cấp. Vui lòng nạp thêm xu', 'error');
    }

  }

  _chooseCity(id: any) {
    this._address.getAllDistrict(id).subscribe(res => this.districts = res, err => console.log(err));
  }

  doUpgradeCPM() {
    if (this.upgradeCPMForm.dirty && this.upgradeCPMForm.valid) {
      this._user.upgradeCPM(this.upgradeCPMForm.value)
        .subscribe(
        res => {
          this.userData = this._user._add_token_key(res);
          (<any>$('#upgradeCPM')).modal('hide');
          location.reload();
        },
        error => { this._notif.swal('Lỗi: ' + error.message, 'error'); });
    }
  }

  // upgradeCTV() {
  //   if (this.userData.credit >= 50) {
  //     this._user.upgradeCTV()
  //       .subscribe(
  //       res => {
  //         this.userData = this._user._add_token_key(res);
  //         location.reload();
  //       },
  //       error => { this._notif.swal('Lỗi: ' + error.message, 'error'); });
  //   } else {
  //     this._notif.swal('Bạn không đủ xu để nâng cấp. Vui lòng nạp thêm xu', 'error');
  //   }
  // }

  extendCPM() {
    if (this.userData.credit >= this.xu) {
      this._notif.swalConfirm('Gia hạn tài khoản', 'Bạn có muốn gia hạn không?', (result) => {
        if (result) {
          this._user.extendCPM()
            .subscribe(
            res => {
              this.userData = this._user._add_token_key(res);
              this._notif.swal(`Bạn đã gia hạn thành công. Hệ thống sẽ cập nhật ngay lập tức`, 'success');
            },
            error => { this._notif.swal('Lỗi: ' + error.message, 'error'); });
        }
      });
    } else {
      //alert('Bạn không đủ xu để nâng cấp. Vui lòng nạp thêm xu');
      this._notif.swal('Bạn không đủ xu để gia hạn. Vui lòng nạp thêm xu', 'error');
    }
  }

  _doSubmitRequestResetPassword(email) {
    this.submitting = true;
    this._user.requestResetPassword(email)
      .subscribe(
      res => {
        this.isRequestedPassword = true;
        this.submitting = false;
        this._notif.swal(`Code xác nhận được gửi đến điạ chỉ Email của bạn. Vui lòng kiểm tra Email`, 'success');
      },
      error => { this._notif.swal('Lỗi: ' + error.message, 'error'); this.submitting = false; });
  }

  _doSubmitResetPassword(email, code, password) {
    this.submitting = true;
    this._user.resetPassword(email, code, password)
      .subscribe(
      res => {
        (<any>$('#resetPassword')).modal('hide');
        this.isRequestedPassword = false;
        this.submitting = false;
        this._notif.swal(`Đổi mật khẩu thành công`, 'success');
      },
      error => { this._notif.swal('Lỗi: ' + error.message, 'error'); this.submitting = false; });
  }

  _haveCode() {
    this.isRequestedPassword = !this.isRequestedPassword;
  }

}
