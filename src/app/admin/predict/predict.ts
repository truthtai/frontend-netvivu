import {Component} from 'angular2/core';
import {JwtHelper} from 'angular2-jwt';
import {RouteConfig, CanActivate} from 'angular2/router';

import {PredictCreate} from './create/create';
import {PredictHome} from './home/home';
import {PredictEdit} from './edit/edit';

@Component({
  selector: 'admin-control-panel-predict',
  template: require('./predict.html'),
  styles: [require('./predict.scss')],
  providers: [RouteConfig]
})

// @CanActivate(() => { return UserService.checkLogin() })
// @RouteConfig([
//   { path: '/admin', component: Home, as: 'Home' }
// ])
@RouteConfig([
  { path: '/', component: PredictHome, as: 'PredictHome', useAsDefault: true },
  { path: '/create', component: PredictCreate, as: 'PredictCreate' },
  { path: '/edit/:id', component: PredictEdit, as: 'PredictEdit' }
])

export class Predict {
  Predicts: Array<any>;
  tournamentForm: any;
  constructor() {
  }

  ngOnInit() {

  }


}
