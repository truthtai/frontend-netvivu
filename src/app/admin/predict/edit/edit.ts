import {Component, NgZone} from 'angular2/core';
import {NgClass, NgIf, FormBuilder, Validators} from 'angular2/common';
import {PredictService} from '../../../shared/services/predict';
import { ValidationService} from '../../../shared/services/validation';
import { SweetAlertService } from '../../../shared/services/sweet-alert';
import {CONFIG} from '../../../config';
import {Router, RouteParams} from 'angular2/router';
// import {CKEDITOR} from 'ckeditor/ckeditor';
// import { NotificationsService } from 'angular2-notifications/components';
import * as moment from 'moment';
declare var window: any;
@Component({
  selector: 'predict-edit',
  template: require('./edit.html'),
  styles: [require('./edit.scss')],
  // directives: [CKEditor],
  providers: [PredictService, SweetAlertService]
})

export class PredictEdit {
  tournamentEditForm: any = {};
  ckeditorContent: any;
  tournamentSelected: any = { teams: {} };
  info: any;

  constructor(
    // public _US: UserService,
    public _PS: PredictService,
    public _router: Router,
    private _fb: FormBuilder,
    public _params: RouteParams,
    public _ngZone: NgZone,
    public _notif: SweetAlertService
  // public _Notif: NotificationsService
    ) {
    // console.log(_params.get('id'));
    // this.ckeditorContent = '<p></p>';
    // this.tournamentSelected = {};
    this.tournamentEditForm = this._fb.group({
      'name': ['', Validators.required],
      'startRun': ['', Validators.required],
      'endRun': ['', Validators.required],
      'credit': ['', Validators.required],
      'teamA': ['', Validators.required],
      'teamB': ['', Validators.required],
    });
    this._PS.get(_params.get('id')).subscribe(
      (res: any) => {
        res.startRun = moment(res.startRun).format().replace('+07:00', '');
        res.endRun = moment(res.endRun).format().replace('+07:00', '');
        this.tournamentSelected = res;
        // console.log(this.tournamentSelected);
      },
      error => { }
      );


  }

  doSubmit() {
    var info = {
      startRun: moment(this.tournamentEditForm.value.startRun).unix(),
      endRun: moment(this.tournamentEditForm.value.endRun).unix(),
      teams: { teamA: this.tournamentEditForm.value.teamA, teamB: this.tournamentEditForm.value.teamB },
      credit: this.tournamentEditForm.value.credit,
      name: this.tournamentEditForm.value.name
    };
    this._PS.edit(info, this._params.get('id'))
      .subscribe(
      res => {
        this._router.navigate(['PredictHome']);
        this._notif.swal('Cập nhật trận đấu thành công', 'success');
      },
      error => {
        this._notif.swal('Có lỗi xảy ra ' + error, 'error');
      });
  }
}
