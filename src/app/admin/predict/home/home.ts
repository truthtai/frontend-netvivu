import {Component} from 'angular2/core';
import {PredictService} from '../../../shared/services/predict';
import { SweetAlertService } from '../../../shared/services/sweet-alert';
import {DateFormatPipe} from 'angular2-moment';

@Component({
  selector: 'predict-home',
  template: require('./home.html'),
  styles: [require('./home.scss')],
  providers: [PredictService, SweetAlertService],
  pipes: [DateFormatPipe]
})

export class PredictHome {
  Predicts: any = [];
  constructor(
    // public _US: UserService,
    public _PS: PredictService,
    public _notif: SweetAlertService
    ) {
    this.getPredict();
  }

  getPredict() {
    this._PS.getAllPredict('all')
      .subscribe(res => {
      this.Predicts = res;
    }, error => {
        this._notif.swal('Có lỗi xảy ra ' + error, 'error');
      });
  }
  doDelete(id: string) {

    this._notif.swalConfirm('Xóa trận đấu', 'Bạn có muốn xóa trận đấu này không?', (result) => {
      if (result) {
        this._PS.delete(id)
          .subscribe(res => {

          this._notif.swal('Xóa trận đấu thành công', 'success');
        }, error => {
            this._notif.swal('Có lỗi xảy ra ' + error, 'error');
          });
      }
    });
  }

  setWin(predict: any, select: any) {
    let colorTeam;
    if (select === 'teamA') {
      colorTeam = 'blue';
    } else {
      colorTeam = 'red';
    }
    this._notif.swalConfirm('Công bố kết quả', `<p>Trận đấu: <strong>${predict.name}</strong></p>
    <p> Đội thắng là: <strong style="color: ${colorTeam}">${predict.teams[select]}</strong> </p>`, (result) => {
        if (result) {
          this._PS.setWin(predict._id, select).subscribe(
            res => {
              this._notif.swal('Công bô kết quả thành công', 'success');
              this.getPredict();
            }
            , err => {
              this._notif.swal('Có lỗi xảy ra: ' + err.message, 'err');
            });
        }
      });
  }
  // doExport(id: string) {
  //   this._PS.exportTournament(id);
  // }
}
