import {Component, NgZone} from 'angular2/core';
import {NgClass, NgIf, FormBuilder, Validators} from 'angular2/common';
import {PredictService} from '../../../shared/services/predict';
import { ValidationService} from '../../../shared/services/validation';
import { SweetAlertService } from '../../../shared/services/sweet-alert';
import {Router} from 'angular2/router';
// import {CKEDITOR} from 'ckeditor/ckeditor';
// import { NotificationsService } from 'angular2-notifications/components';
// import {UNITYTinyMCE} from '../../../shared/tinymce/tinymce';
import * as moment from 'moment';
import {SelectDateTime} from '../../../shared/directives/date';

declare var $: any;
declare var window: any;
@Component({
  selector: 'predict-create',
  template: require('./create.html'),
  styles: [require('./create.scss')],
  directives: [SelectDateTime],
  providers: [PredictService, SweetAlertService]
})

export class PredictCreate {
  tournamentForm: any;
  ckeditorContent: any;
  info: any;
  file: any;
  imageReview: any;
  selectedFile: any = '';
  constructor(
    // public _US: UserService,
    public _PS: PredictService,
    public _router: Router,
    private _fb: FormBuilder,
    public _notif: SweetAlertService,
    public _ngZone: NgZone
    ) {


    this.ckeditorContent = '<p></p>';
    this.tournamentForm = this._fb.group({
      'name': ['', Validators.required],
      'startRun': ['', Validators.required],
      'endRun': ['', Validators.required],
      'credit': ['', Validators.required],
      'teamA': ['', Validators.required],
      'teamB': ['', Validators.required],
    });
  }

  ngOnInit() {
    // let editor = window.CKEDITOR.replace('editor1');
    // editor.on('change', (ev) => {
    //   this._ngZone.run(() => {
    //     this.tournamentForm.value.description = ev.editor.getData();
    //   });
    // });
  }

  doSubmit() {
    var info = {
      startRun: moment(this.tournamentForm.value.startRun).unix(),
      endRun: moment(this.tournamentForm.value.endRun).unix(),
      credit: this.tournamentForm.value.credit,
      teams: { teamA: this.tournamentForm.value.teamA, teamB: this.tournamentForm.value.teamB },
      name: this.tournamentForm.value.name,
    };

    // this._TournamentService.addTournament(info)
    //   .subscribe(
    //   res => {
    //     this._router.navigate(['TournamentHome']);
    //     this._notif.swal('Tạo trận đấu thành công', 'success');
    //   },
    //   error => {
    //     this._notif.swal('Có lỗi xảy ra ' + error, 'error');
    //   });
    // if (this.tournamentForm.dirty && this.tournamentForm.valid) {
    this._PS.add(info).subscribe(res => {
      this._router.navigate(['PredictHome']);
      this._notif.swal('Tạo trận đấu dự đoán thành công', 'success');
    },
      error => {
        this._notif.swal('Có lỗi xảy ra ' + error, 'error');
      });
    //  }
  }
}
