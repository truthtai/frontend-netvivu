import {Component} from 'angular2/core';
import {NewsService} from '../../../shared/services/news';
import { SweetAlertService } from '../../../shared/services/sweet-alert';

@Component({
  selector: 'tournament-home',
  template: require('./home.html'),
  styles: [require('./home.scss')],
  providers: [NewsService, SweetAlertService]
})

export class NewsHome {
  News: Array<any>;
  constructor(
    // public _US: UserService,
    public _NewsService: NewsService,
    public _notif: SweetAlertService
    ) {

    this._NewsService.getAllNews().subscribe(res => { this.News = <any>res; }, error => { });

  }

  doDelete(id: string) {

    this._notif.swalConfirm('Xóa tin tức', 'Bạn có muốn xóa tin này không?', (result) => {
      if (result) {
        this._NewsService.deleteNews(id)
          .subscribe(res => {
          this.News = this.News.filter((r) => r._id !== id);
          this._notif.swal('Xóa tin thành công', 'success');
        }, error => {
            this._notif.swal('Có lỗi xảy ra ' + error, 'error');
          });
      }
    });
  }
}
