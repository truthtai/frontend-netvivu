import {Component, NgZone} from 'angular2/core';
import {NgClass, NgIf, FormBuilder, Validators} from 'angular2/common';
import {NewsService} from '../../../shared/services/news';
import { ValidationService} from '../../../shared/services/validation';
import { SweetAlertService } from '../../../shared/services/sweet-alert';
// import {CKEditor} from 'ng2-ckeditor';
import {Router, RouteParams} from 'angular2/router';
// import {CKEDITOR} from 'ckeditor/ckeditor';
// import { NotificationsService } from 'angular2-notifications/components';
import * as moment from 'moment';
declare var window: any;
@Component({
  selector: 'news-edit',
  template: require('./edit.html'),
  styles: [require('./edit.scss')],
  // directives: [CKEditor],
  providers: [NewsService, SweetAlertService]
})

export class NewsEdit {
  newsEditForm: any = {};
  ckeditorContent: any;
  newsSelected: any = {};
  info: any;

  constructor(
    // public _US: UserService,
    public _NewsService: NewsService,
    public _router: Router,
    private _fb: FormBuilder,
    public _params: RouteParams,
    public _ngZone: NgZone,
    public _notif: SweetAlertService
  // public _Notif: NotificationsService
    ) {
    // console.log(_params.get('id'));
    // this.ckeditorContent = '<p></p>';
    // this.tournamentSelected = {};
    this.newsEditForm = this._fb.group({
      'name': ['', Validators.required],
      'description': ['', Validators.required]
    });
    this._NewsService.getNews(_params.get('id')).subscribe(
      (res: any) => {
        this.newsSelected = res;
      },
      error => { }
      );


  }

  ngOnInit() {
    let editor = window.CKEDITOR.replace('editor1');
    editor.on('change', (ev) => {
      this._ngZone.run(() => {
        this.newsSelected.description = ev.editor.getData();
      });
    });
  }



  doSubmit() {
    this._NewsService.editNews(this.newsEditForm.value, this._params.get('id'))
      .subscribe(
      res => {
        this._router.navigate(['NewsHome']);
        this._notif.swal('Cập nhật tin tức thành công', 'success');
      },
      error => {
        this._notif.swal('Có lỗi xảy ra ' + error, 'error');
      });
  }
}
