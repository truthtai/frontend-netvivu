import {Component} from 'angular2/core';
import {JwtHelper} from 'angular2-jwt';
import {RouteConfig, CanActivate} from 'angular2/router';

import {NewsCreate} from './create/create';
import {NewsHome} from './home/home';
import {NewsEdit} from './edit/edit';

@Component({
  selector: 'admin-control-panel-news',
  template: require('./news.html'),
  styles: [require('./news.scss')],
  providers: [RouteConfig]
})

// @CanActivate(() => { return UserService.checkLogin() })
// @RouteConfig([
//   { path: '/admin', component: Home, as: 'Home' }
// ])
@RouteConfig([
  { path: '/', component: NewsHome, as: 'NewsHome', useAsDefault: true },
  { path: '/create', component: NewsCreate, as: 'NewsCreate' },
  { path: '/edit/:id', component: NewsEdit, as: 'NewsEdit' }
])

export class News {
  constructor() { }
}
