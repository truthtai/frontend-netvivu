import {Component, NgZone} from 'angular2/core';
import {NgClass, NgIf, FormBuilder, Validators} from 'angular2/common';
import {NewsService} from '../../../shared/services/news';
import { ValidationService} from '../../../shared/services/validation';
import { SweetAlertService } from '../../../shared/services/sweet-alert';
import {Router} from 'angular2/router';
// import {CKEDITOR} from 'ckeditor/ckeditor';
// import { NotificationsService } from 'angular2-notifications/components';
// import {UNITYTinyMCE} from '../../../shared/tinymce/tinymce';
declare var $: any;
declare var window: any;
@Component({
  selector: 'tournament-create',
  template: require('./create.html'),
  styles: [require('./create.scss')],
  // directives: [UNITYTinyMCE],
  providers: [NewsService, SweetAlertService]
})

export class NewsCreate {
  newsForm: any;
  ckeditorContent: any;
  info: any;
  constructor(
    // public _US: UserService,
    public _NewsService: NewsService,
    public _router: Router,
    private _fb: FormBuilder,
    public _notif: SweetAlertService,
    public _ngZone: NgZone
    ) {


    this.ckeditorContent = '<p></p>';
    this.newsForm = this._fb.group({
      'name': ['', Validators.required],
      'description': ['', Validators.required]
    });

  }

  ngOnInit() {
    let editor = window.CKEDITOR.replace('editor1');
    editor.on('change', (ev) => {
      this._ngZone.run(() => {
        this.newsForm.value.description = ev.editor.getData();
      });
    });
  }

  doSubmit() {
    this._NewsService.addNews(this.newsForm.value)
      .subscribe(
      res => {
        this._router.navigate(['NewsHome']);
        this._notif.swal('Tạo tin tức thành công', 'success');
      },
      error => {
        this._notif.swal('Có lỗi xảy ra ' + error, 'error');
      });
  }

}
