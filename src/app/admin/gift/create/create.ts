import {Component, NgZone} from 'angular2/core';
import {NgClass, NgIf, FormBuilder, Validators} from 'angular2/common';
import {GiftService} from '../../../shared/services/gift';
import { ValidationService} from '../../../shared/services/validation';
import { SweetAlertService } from '../../../shared/services/sweet-alert';
import {Router} from 'angular2/router';
import * as moment from 'moment';
import {SelectDateTime} from '../../../shared/directives/date';

declare var $: any;
declare var window: any;
@Component({
  selector: 'gift-create',
  template: require('./create.html'),
  styles: [require('./create.scss')],
  directives: [SelectDateTime],
  providers: [GiftService, SweetAlertService]
})

export class GiftCreate {
  giftForm: any;
  giftCredit: any = 0;
  giftDes: any = '<p></p>';
  ckeditorContent: any;
  info: any;
  file: any;
  imageReview: any;
  selectedFile: any = '';
  constructor(
    // public _US: UserService,
    public _GS: GiftService,
    public _router: Router,
    private _fb: FormBuilder,
    public _notif: SweetAlertService,
    public _ngZone: NgZone
    ) {


    this.ckeditorContent = '<p></p>';
    this.giftForm = this._fb.group({
      'name': ['', Validators.required],
      'status': ['', Validators.required],
      'price': ['', Validators.required],
      'category': ['', Validators.required],
      'subCategory': ['', Validators.required]
    });
  }

  ngOnInit() {
    let editor = window.CKEDITOR.replace('editor1');
    editor.on('change', (ev) => {
      this._ngZone.run(() => {
        this.giftDes = ev.editor.getData();
      });
    });
  }

  doSubmit() {
    var info = {
      credit: this.giftCredit,
      status: this.giftForm.value.status,
      description: this.giftDes,
      name: this.giftForm.value.name,
      price: this.giftForm.value.price,
      category: this.giftForm.value.category,
      subCategory: this.giftForm.value.subCategory,
      image: this.file
    };

    this._GS.add(info).then(res => {
      this._router.navigate(['GiftHome']);
      this._notif.swal('Tạo Quà thành công', 'success');
    },
      error => {
        this._notif.swal('Có lỗi xảy ra ' + error, 'error');
      });
    //  }
  }

  selectFile($event): void {
    var inputValue = $event.target;
    this.file = inputValue.files[0];
    var reader = new FileReader();
    reader.onload = () => {
      this.imageReview = reader.result;
    };
    if (this.file) {
      reader.readAsDataURL(this.file);
    }
    //console.debug("Input File name: " + this.file.name + " type:" + this.file.size + " size:" + this.file.size);
  }

  _change(price: any) {
    this.giftCredit = (price / 1000) + (((price / 1000) * 30) / 100);
  }

}
