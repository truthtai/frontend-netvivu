import {Component} from 'angular2/core';
import {JwtHelper} from 'angular2-jwt';
import {RouteConfig, CanActivate} from 'angular2/router';

import {GiftCreate} from './create/create';
import {GiftHome} from './home/home';
import {GiftEdit} from './edit/edit';

@Component({
  selector: 'admin-control-panel-gift',
  template: require('./gift.html'),
  styles: [require('./gift.scss')],
  providers: [RouteConfig]
})

// @CanActivate(() => { return UserService.checkLogin() })
// @RouteConfig([
//   { path: '/admin', component: Home, as: 'Home' }
// ])
@RouteConfig([
  { path: '/', component: GiftHome, as: 'GiftHome', useAsDefault: true },
  { path: '/create', component: GiftCreate, as: 'GiftCreate' },
  { path: '/edit/:id', component: GiftEdit, as: 'GiftEdit' }
])

export class Gift {
  constructor() { }
  ngOnInit() { }
}
