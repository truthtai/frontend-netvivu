import {Component} from 'angular2/core';
import {GiftService} from '../../../shared/services/gift';
import {OrderService} from '../../../shared/services/order';
import { SweetAlertService } from '../../../shared/services/sweet-alert';
import {DateFormatPipe} from 'angular2-moment';

@Component({
  selector: 'gift-home',
  template: require('./home.html'),
  styles: [require('./home.scss')],
  providers: [GiftService, SweetAlertService, OrderService],
  pipes: [DateFormatPipe]
})

export class GiftHome {
  Gifts: Array<any>;
  Orders: Array<any>;
  selectedOrder: any = {};
  noteOrder: any;
  statusOrder: any;
  constructor(
    // public _US: UserService,
    public _GS: GiftService,
    public _OS: OrderService,
    public _notif: SweetAlertService
    ) {
    this.getAllGift();
    this.getAllOrder();

  }

  getAllGift() {
    this._GS.getAll().subscribe(res => { this.Gifts = <any>res; }, error => { });
  }

  getAllOrder() {
    this._OS.getAll().subscribe(res => { this.Orders = <any>res; }, error => { });
  }

  doDelete(id: string) {

    this._notif.swalConfirm('Xóa Quà', 'Bạn có muốn xóa Quà này không?', (result) => {
      if (result) {
        this._GS.delete(id)
          .subscribe(res => {
          this.getAllGift();
          this._notif.swal('Xóa Quà thành công', 'success');
        }, error => {
            this._notif.swal('Có lỗi xảy ra ' + error, 'error');
          });
      }
    });
  }

  _selectedOrder(order: any) {
    (<any>$('#orderReview')).modal('show');
    this.selectedOrder = order;
  }

  doDeleteOrder(id: String) {
    this._notif.swalConfirm('Xóa đơn hàng', 'Bạn có muốn xóa đơn hàng này không?', (result) => {
      if (result) {
        this._OS.delete(id)
          .subscribe(res => {
          this.getAllOrder();
          this._notif.swal('Xóa thành công', 'success');
        }, error => {
            this._notif.swal('Có lỗi xảy ra ' + error, 'error');
          });
      }
    });
  }

  _updateOrder(status: any) {
    this.statusOrder = status;
    this.selectedOrder.status = status;
  }

  doSubmitUpdateOrder() {
    var info = {
      status: this.statusOrder,
      note: this.noteOrder
    };
    this._notif.swalConfirm('Cập nhật đơn hàng', 'Bạn có muốn cập nhật đơn hàng này không?', (result) => {
      if (result) {
        this._OS.edit(info, this.selectedOrder._id)
          .subscribe(res => {
          this.getAllOrder();
          this._notif.swal('Cập nhật thành công', 'success');
        }, error => {
            this._notif.swal('Có lỗi xảy ra ' + error, 'error');
          });
      }
    });
  }
}
