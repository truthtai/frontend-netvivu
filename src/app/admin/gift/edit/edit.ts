import {Component, NgZone} from 'angular2/core';
import {NgClass, NgIf, FormBuilder, Validators} from 'angular2/common';
import {GiftService} from '../../../shared/services/gift';
import { ValidationService} from '../../../shared/services/validation';
import { SweetAlertService } from '../../../shared/services/sweet-alert';
import {Router, RouteParams} from 'angular2/router';
import * as moment from 'moment';
import {SelectDateTime} from '../../../shared/directives/date';
import {CONFIG} from '../../../config';

declare var window: any;
@Component({
  selector: 'tournament-edit',
  template: require('./edit.html'),
  styles: [require('./edit.scss')],
  // directives: [CKEditor],
  providers: [GiftService, SweetAlertService]
})

export class GiftEdit {
  giftCredit: any;
  giftForm: any = {};
  ckeditorContent: any;
  giftSelected: any = {};
  info: any;

  constructor(
    // public _US: UserService,
    public _GS: GiftService,
    public _router: Router,
    private _fb: FormBuilder,
    public _params: RouteParams,
    public _ngZone: NgZone,
    public _notif: SweetAlertService
  // public _Notif: NotificationsService
    ) {
    // console.log(_params.get('id'));
    // this.ckeditorContent = '<p></p>';
    // this.tournamentSelected = {};
    this.giftForm = this._fb.group({
      'name': ['', Validators.required],
      'status': ['', Validators.required],
      'price': ['', Validators.required],
      'description': ['', Validators.required],
      'category': ['', Validators.required],
      'subCategory': ['', Validators.required],
    });

    this._GS.get(_params.get('id')).subscribe(
      (res: any) => {
        res.image = `${CONFIG.API_URL}/image/${res.image}`;
        if (!res.subCategory) { res.subCategory = ''; }
        this.giftSelected = res;
        this._change(res.price);
        this._EDITOR();
      },
      error => { }
      );


  }

  _EDITOR() {
    let editor = window.CKEDITOR.replace('editor1');
    editor.on('change', (ev) => {
      this._ngZone.run(() => {
        this.giftSelected.description = ev.editor.getData();
      });
    });
  }



  doSubmit() {
    var info = {
      credit: this.giftCredit,
      status: this.giftForm.value.status,
      description: this.giftForm.value.description,
      name: this.giftForm.value.name,
      price: this.giftForm.value.price,
      category: this.giftForm.value.category,
      subCategory: this.giftForm.value.subCategory,
      // image: this.file
    };
    this._GS.edit(info, this._params.get('id'))
      .subscribe(
      res => {
        this._router.navigate(['GiftHome']);
        this._notif.swal('Cập nhật Quà thành công', 'success');
      },
      error => {
        this._notif.swal('Có lỗi xảy ra ' + error, 'error');
      });
  }

  _change(price: any) {
    this.giftCredit = (price / 1000) + (((price / 1000) * 30) / 100);
  }
}
