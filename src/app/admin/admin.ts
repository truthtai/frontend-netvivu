import {Component} from 'angular2/core';
import {JwtHelper} from 'angular2-jwt';
import {UserService} from '../shared/services/user';
import {CanActivate} from 'angular2/router';
// import {CardItem} from '../cardItem/cardItem';
import {RouteConfig, Router, ROUTER_DIRECTIVES} from 'angular2/router';
import {NgClass, NgIf, FORM_PROVIDERS, FormBuilder, Validators, FORM_DIRECTIVES} from 'angular2/common';
import { ValidationService} from '../shared/services/validation';
import {User} from './user/user';
import {Tournament} from './tournament/tournament';
import {News} from './news/news';
import {Gift} from './gift/gift';
import {Predict} from './predict/predict';
import * as _ from 'lodash';
import {RouterActive} from '../directives/router-active';
@Component({
  selector: 'admin-control-panel',
  template: require('./admin.html'),
  styles: [require('./admin.scss')],
  providers: [JwtHelper, UserService],
  directives: [NgClass, NgIf, FORM_DIRECTIVES, ROUTER_DIRECTIVES, RouterActive, User, Tournament]
})

// @CanActivate(() => { return UserService.checkLogin() })
@RouteConfig([
  { path: '/', redirectTo: ['User'] },
  { path: '/user', component: User, as: 'User' },
  { path: '/tournament/...', component: Tournament, as: 'TournamentAdmin' },
  { path: '/news/...', component: News, as: 'NewsAdmin' },
  { path: '/gift/...', component: Gift, as: 'GiftAdmin' },
  { path: '/du-doan/...', component: Predict, as: 'PredictAdmin' }
])


export class Admin {
  constructor(public _router: Router, public _US: UserService) {
    if (this._US.getTypeUser() && this._US.getTypeUser() !== 'admin') {
      this._router.navigateByUrl('/not-logged');
    }
  }
  // isActive(instruction: any[]): boolean {
  //   return this._router.isRouteActive(this._router.generate(instruction));
  // }
}
