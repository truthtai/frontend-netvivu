import {Component} from 'angular2/core';
import {TournamentService} from '../../../shared/services/tournament';
import { SweetAlertService } from '../../../shared/services/sweet-alert';
import {DateFormatPipe} from 'angular2-moment';

@Component({
  selector: 'tournament-home',
  template: require('./home.html'),
  styles: [require('./home.scss')],
  providers: [TournamentService, SweetAlertService],
  pipes: [DateFormatPipe]
})

export class TournamentHome {
  Tournaments: Array<any>;
  constructor(
    // public _US: UserService,
    public _TournamentService: TournamentService,
    public _notif: SweetAlertService
    ) {

    this._TournamentService.getAllTournament().subscribe(res => { this.Tournaments = <any>res; }, error => { });

  }

  doDelete(id: string) {

    this._notif.swalConfirm('Xóa trận đấu', 'Bạn có muốn xóa trận đấu này không?', (result) => {
      if (result) {
        this._TournamentService.deleteTournament(id)
          .subscribe(res => {
          this.Tournaments = this.Tournaments.filter((r) => r._id !== id);
          this._notif.swal('Xóa trận đấu thành công', 'success');
        }, error => {
            this._notif.swal('Có lỗi xảy ra ' + error, 'error');
          });
      }
    });
  }

  doExport(id: string) {
    this._TournamentService.exportTournament(id);
  }
}
