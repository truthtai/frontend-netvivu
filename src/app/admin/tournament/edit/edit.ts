import {Component, NgZone} from 'angular2/core';
import {NgClass, NgIf, FormBuilder, Validators} from 'angular2/common';
import {TournamentService} from '../../../shared/services/tournament';
import { ValidationService} from '../../../shared/services/validation';
import { SweetAlertService } from '../../../shared/services/sweet-alert';
import {CONFIG} from '../../../config';
import {Router, RouteParams} from 'angular2/router';
// import {CKEDITOR} from 'ckeditor/ckeditor';
// import { NotificationsService } from 'angular2-notifications/components';
import * as moment from 'moment';
declare var window: any;
@Component({
  selector: 'tournament-edit',
  template: require('./edit.html'),
  styles: [require('./edit.scss')],
  // directives: [CKEditor],
  providers: [TournamentService, SweetAlertService]
})

export class TournamentEdit {
  tournamentEditForm: any = {};
  ckeditorContent: any;
  tournamentSelected: any = {};
  info: any;

  constructor(
    // public _US: UserService,
    public _TournamentService: TournamentService,
    public _router: Router,
    private _fb: FormBuilder,
    public _params: RouteParams,
    public _ngZone: NgZone,
    public _notif: SweetAlertService
  // public _Notif: NotificationsService
    ) {
    // console.log(_params.get('id'));
    // this.ckeditorContent = '<p></p>';
    // this.tournamentSelected = {};
    this.tournamentEditForm = this._fb.group({
      'name': ['', Validators.required],
      'startReg': ['', Validators.required],
      'endReg': ['', Validators.required],
      'startRun': ['', Validators.required],
      'endRun': ['', Validators.required],
      'remindTime': ['', Validators.required],
      'randomTime': ['', Validators.required],
      'credit': ['', Validators.required],
      'freeFirstTime': [''],
      'description': ['', Validators.required]
    });
    this._TournamentService.getTournament(_params.get('id')).subscribe(
      (res: any) => {
        res.startReg = moment(res.startReg).format().replace('+07:00', '');
        res.endReg = moment(res.endReg).format().replace('+07:00', '');
        res.startRun = moment(res.startRun).format().replace('+07:00', '');
        res.endRun = moment(res.endRun).format().replace('+07:00', '');
        res.remindTime = moment(res.remindTime).format().replace('+07:00', '');
        res.randomTime = moment(res.randomTime).format().replace('+07:00', '');
        res.image = `${CONFIG.API_URL}/image/${res.image}`;
        this.tournamentSelected = res;
        // console.log(this.tournamentSelected);
      },
      error => { }
      );


  }

  ngOnInit() {
    let editor = window.CKEDITOR.replace('editor1');
    editor.on('change', (ev) => {
      this._ngZone.run(() => {
        this.tournamentSelected.description = ev.editor.getData();
      });
    });
  }



  doSubmit() {
    var info = {
      startReg: moment(this.tournamentEditForm.value.startReg).unix(),
      endReg: moment(this.tournamentEditForm.value.endReg).unix(),
      startRun: moment(this.tournamentEditForm.value.startRun).unix(),
      endRun: moment(this.tournamentEditForm.value.endRun).unix(),
      remindTime: moment(this.tournamentEditForm.value.remindTime).unix(),
      randomTime: moment(this.tournamentEditForm.value.randomTime).unix(),
      credit: this.tournamentEditForm.value.credit,
      freeFirstTime: this.tournamentEditForm.value.freeFirstTime,
      description: this.tournamentEditForm.value.description,
      name: this.tournamentEditForm.value.name
    };
    this._TournamentService.editTournament(info, this._params.get('id'))
      .subscribe(
      res => {
        this._router.navigate(['TournamentHome']);
        this._notif.swal('Cập nhật trận đấu thành công', 'success');
      },
      error => {
        this._notif.swal('Có lỗi xảy ra ' + error, 'error');
      });
  }
}
