import {Component} from 'angular2/core';
import {JwtHelper} from 'angular2-jwt';
import {RouteConfig, CanActivate} from 'angular2/router';

import {TournamentCreate} from './create/create';
import {TournamentHome} from './home/home';
import {TournamentEdit} from './edit/edit';

@Component({
  selector: 'admin-control-panel-tournament',
  template: require('./tournament.html'),
  styles: [require('./tournament.scss')],
  providers: [RouteConfig]
})

// @CanActivate(() => { return UserService.checkLogin() })
// @RouteConfig([
//   { path: '/admin', component: Home, as: 'Home' }
// ])
@RouteConfig([
  { path: '/', component: TournamentHome, as: 'TournamentHome', useAsDefault: true },
  { path: '/create', component: TournamentCreate, as: 'TournamentCreate' },
  { path: '/edit/:id', component: TournamentEdit, as: 'TournamentEdit' }
])

export class Tournament {
  Tournaments: Array<any>;
  tournamentForm: any;
  constructor() {
  }

  ngOnInit() {

  }


}
