import {Component, NgZone} from 'angular2/core';
import {NgClass, NgIf, FormBuilder, Validators} from 'angular2/common';
import {TournamentService} from '../../../shared/services/tournament';
import { ValidationService} from '../../../shared/services/validation';
import { SweetAlertService } from '../../../shared/services/sweet-alert';
import {Router} from 'angular2/router';
// import {CKEDITOR} from 'ckeditor/ckeditor';
// import { NotificationsService } from 'angular2-notifications/components';
// import {UNITYTinyMCE} from '../../../shared/tinymce/tinymce';
import * as moment from 'moment';
import {SelectDateTime} from '../../../shared/directives/date';

declare var $: any;
declare var window: any;
@Component({
  selector: 'tournament-create',
  template: require('./create.html'),
  styles: [require('./create.scss')],
  directives: [SelectDateTime],
  providers: [TournamentService, SweetAlertService]
})

export class TournamentCreate {
  tournamentForm: any;
  ckeditorContent: any;
  info: any;
  file: any;
  imageReview: any;
  selectedFile: any = '';
  constructor(
    // public _US: UserService,
    public _TournamentService: TournamentService,
    public _router: Router,
    private _fb: FormBuilder,
    public _notif: SweetAlertService,
    public _ngZone: NgZone
    ) {


    this.ckeditorContent = '<p></p>';
    this.tournamentForm = this._fb.group({
      'name': ['', Validators.required],
      'startReg': ['', Validators.required],
      'endReg': ['', Validators.required],
      'startRun': ['', Validators.required],
      'endRun': ['', Validators.required],
      'remindTime': ['', Validators.required],
      'randomTime': ['', Validators.required],
      'credit': ['', Validators.required],
      'freeFirstTime': ['', Validators.required],
      'description': ['', Validators.required],
      'game': ['', Validators.required]
    });

    this.tournamentForm.value.endReg = new Date();
  }

  ngOnInit() {
    let editor = window.CKEDITOR.replace('editor1');
    editor.on('change', (ev) => {
      this._ngZone.run(() => {
        this.tournamentForm.value.description = ev.editor.getData();
      });
    });
  }

  doSubmit() {
    var info = {
      startReg: moment(this.tournamentForm.value.startReg).unix(),
      endReg: moment(this.tournamentForm.value.endReg).unix(),
      startRun: moment(this.tournamentForm.value.startRun).unix(),
      endRun: moment(this.tournamentForm.value.endRun).unix(),
      remindTime: moment(this.tournamentForm.value.remindTime).unix(),
      randomTime: moment(this.tournamentForm.value.randomTime).unix(),
      credit: this.tournamentForm.value.credit,
      freeFirstTime: this.tournamentForm.value.freeFirstTime,
      description: this.tournamentForm.value.description,
      name: this.tournamentForm.value.name,
      game: this.tournamentForm.value.game,
      image: this.file
    };

    // this._TournamentService.addTournament(info)
    //   .subscribe(
    //   res => {
    //     this._router.navigate(['TournamentHome']);
    //     this._notif.swal('Tạo trận đấu thành công', 'success');
    //   },
    //   error => {
    //     this._notif.swal('Có lỗi xảy ra ' + error, 'error');
    //   });
    // if (this.tournamentForm.dirty && this.tournamentForm.valid) {
    this._TournamentService.addTournament(info).then(res => {
      this._router.navigate(['TournamentHome']);
      this._notif.swal('Tạo trận đấu thành công', 'success');
    },
      error => {
        this._notif.swal('Có lỗi xảy ra ' + error, 'error');
      });
    //  }
  }

  selectFile($event): void {
    var inputValue = $event.target;
    this.file = inputValue.files[0];
    var reader = new FileReader();
    reader.onload = () => {
      this.imageReview = reader.result;
    };
    if (this.file) {
      reader.readAsDataURL(this.file);
    }
    //console.debug("Input File name: " + this.file.name + " type:" + this.file.size + " size:" + this.file.size);
  }

}
