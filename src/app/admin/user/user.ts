import {Component} from 'angular2/core';
import {JwtHelper} from 'angular2-jwt';
import {CanActivate} from 'angular2/router';
import {UserService} from '../../shared/services/user';
import {TypeService} from '../../shared/services/type';
import {PermissionService} from '../../shared/services/permission';
import { SweetAlertService } from '../../shared/services/sweet-alert';
import {NgClass, NgIf, FORM_PROVIDERS, FormBuilder, Validators, FORM_DIRECTIVES} from 'angular2/common';
import { ValidationService} from '../../shared/services/validation';
import {DateFormatPipe} from 'angular2-moment';
import * as _ from 'lodash';
import {RouteConfig, Router, ROUTER_DIRECTIVES} from 'angular2/router';

@Component({
  selector: 'admin-control-panel-user',
  template: require('./user.html'),
  styles: [require('./user.scss')],
  providers: [UserService, TypeService, PermissionService, JwtHelper, SweetAlertService],
  directives: [NgClass, NgIf, FORM_DIRECTIVES],
  pipes: [DateFormatPipe]
})

// @CanActivate(() => { return UserService.checkLogin() })
// @RouteConfig([
//   { path: '/admin', component: Home, as: 'Home' }
// ])


export class User {
  Users: Array<any> = []; Types: Array<any>; Permissions: Array<any>;
  editUserForm: any;
  userSelected: any;
  typeSelected: any;
  permission: any;
  histories: any = [];
  Reports: any = [];
  isPerTypesNotSelected: any;
  submitting: any = false;
  userHistory: any = '';
  constructor(
    // public _US: UserService,
    public _router: Router,
    public _UserService: UserService,
    public _TypeService: TypeService,
    public _PermissionService: PermissionService,
    public _notif: SweetAlertService,
    private _fb: FormBuilder
    ) {
    this._resetFormPermission();

    this.editUserForm = this._fb.group({
      'username': ['', Validators.required],
      'type': ['', Validators.required],
      'email': ['', Validators.compose([Validators.required, ValidationService.emailValidator])],
      'phone': ['', Validators.required],
      'name': ['', Validators.required],
    });

    this._getAllUser();
    this._TypeService.getAllType().subscribe(res => { this.Types = <any>res; }, error => { });
    this._PermissionService.getAllPermission().subscribe(res => { this.Permissions = <any>res; }, error => { });

  }

  ngOnInit() {
    console.log();
    this._getReportUser();
  }


  _getAllUser() {
    this._UserService.getAllUser().subscribe(res => { this.Users = <any>res; }, error => { });
  }

  _getReportUser() {
    this._UserService.getReportUser().subscribe(res => { this.Reports = <any>res; }, error => { });
  }

  getLogs() {
    this._UserService.getAllLogs().subscribe(res => { this.histories = <any>res; }, error => { });
  }

  _resetFormPermission() {
    this.permission = { name: '', description: '' };
  }

  editUser(user: any) {
    this.userSelected = user;
  }

  doEdit() {
    if (this.editUserForm.dirty && this.editUserForm.valid) {
      this._UserService.editUserID(this.editUserForm.value, this.userSelected._id)
        .subscribe(
        res => {
          this._getAllUser();
          (<any>$('#editUser')).modal('hide');
          this._notif.swal('Chỉnh sửa thành công', 'success');
        },
        error => { this._notif.swal('Lỗi: ' + error.message, 'error'); });
    }
  }

  addPermission(name: any, description: any) {
    this.permission = { name, description };
    this._PermissionService.addPermission(this.permission).subscribe(res => { this.Permissions.push(res); this._resetFormPermission(); }, error => { });
  }

  searchTour(value: any) {
    this._UserService.getAllUser(value).subscribe(res => { this.Users = <any>res; }, error => { });
  }

  _perTypesNotSelected() {
    this.isPerTypesNotSelected = _.filter(this.Permissions, (obj) => !_.find(this.typeSelected.permission, obj));
  }

  _typeSelected(type) {
    return _.find(this.Types, { '_id': type });
  }

  _permissionSelected(type) {
    return _.find(this.Permissions, { 'description': type });
  }

  selectType(type: any) {
    this.typeSelected = this._typeSelected(type);
    this._perTypesNotSelected();
  }

  addPerToType(per: any) {
    let perSelected = this._permissionSelected(per);
    this.typeSelected.permission.push(perSelected);

    let info = {
      id: this.typeSelected._id,
      name: this.typeSelected.name,
      permission: _.map(this.typeSelected.permission, (e: any) => e._id)
    };

    this._TypeService.editPermissionOfType(info).subscribe(res => {
      this._perTypesNotSelected();
    }, error => { });


  }

  doDeletePer(name: any) {
    this.typeSelected.permission = _.filter(this.typeSelected.permission, (m: any) => m.name !== name);
    let info = {
      id: this.typeSelected._id,
      name: this.typeSelected.name,
      permission: _.map(this.typeSelected.permission, (e: any) => e._id)
    };

    this._TypeService.editPermissionOfType(info).subscribe(res => {
      this._perTypesNotSelected();
    }, error => { });
  }

  doLockUser(id: any) {
    this._notif.swalConfirm('Khóa tài khoản', 'Bạn có muốn khóa tài khoản này không?', (result) => {
      if (result) {
        this._UserService.lockUser(id)
          .subscribe(res => {
          this._notif.swal('Khóa tài khoản thành công', 'success');
          (<any>$('#editUser')).modal('hide');
          this._getAllUser();
        }, error => {
            this._notif.swal('Có lỗi xảy ra ' + error, 'error');
          });
      }
    });
  }

  _extractCredit(username, reason, credit: any, mcredit: any) {
    this.submitting = true;
    const data = {
      username, reason, credit: parseInt(credit), mcredit: parseInt(mcredit), action: ''
    };
    if (credit > 0 && mcredit > 0) {
      this._notif.swal('Chỉ được add 1 hành động thêm hoặc trừ xu', 'error');
      this.submitting = false;
    } else {
      if (!data.credit) data.credit = 0;
      if (!data.mcredit) data.mcredit = 0;
      if (data.credit && data.credit > 0) {
        data.action = 'add';
      }
      if (data.mcredit && data.mcredit > 0) {
        data.action = 'minius';
      }

      this._UserService.extractCredit(data).subscribe(
        res => {
          this._notif.swal('Thực thi thành công', 'success');
          this.submitting = false;
        }
        , error => {
          this._notif.swal('Có lỗi xảy ra ' + error.message, 'error');
          this.submitting = false;
        }
        );
    }
  }

  _searchUserHistory(user: any) {
    this._UserService.getLogByUser(user).subscribe(res => { this.histories = <any>res; }, error => { });
  }


}
