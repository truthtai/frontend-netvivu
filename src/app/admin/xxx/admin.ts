import {Injectable} from 'angular2/core';
import {Http, Response} from 'angular2/http';
import {Observable}     from 'rxjs/Observable';
import {Headers, RequestOptions} from 'angular2/http';
import {AuthHttp, JwtHelper} from 'angular2-jwt';
import {CONFIG} from '../../config';

interface User {
  // accessToken: string;
}

interface Type {
  //message: string;
}

interface Permission {
  // type: string;
}

@Injectable()

export class AdminService {

  constructor(public http: Http, public _authHttp: AuthHttp, public _jwt: JwtHelper) {

  }

  private _API_USER = `${CONFIG.API_URL}/user`;
  private _API_TYPE = `${CONFIG.API_URL}/type`;
  private _API_PERMISSION = `${CONFIG.API_URL}/permission`;
  private _API_TOURNAMENT = `${CONFIG.API_URL}/tournament`;

  getAllUser(): Observable<User> {

    return this._authHttp.get(this._API_USER)
      .map(res => <User>res.json())
      .catch(this.handleError);
  }

  getAllType(): Observable<Type> {

    return this._authHttp.get(this._API_TYPE)
      .map(res => <Type>res.json())
      .catch(this.handleError);
  }

  getAllPermission(): Observable<Permission> {

    return this._authHttp.get(this._API_PERMISSION)
      .map(res => <Permission>res.json())
      .catch(this.handleError);
  }
  
  getAllTournament(): Observable<Permission> {

    return this._authHttp.get(this._API_TOURNAMENT)
      .map(res => <Permission>res.json())
      .catch(this.handleError);
  }

  addPermission(info: any): Observable<Permission> {
    let body = JSON.stringify(info);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this._authHttp.post(this._API_PERMISSION, body, options)
      .map(res => <Permission>res.json())
      .catch(this.handleError);
  }

  editPermissionOfType(info: any): Observable<Permission> {
    let body = JSON.stringify(info);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this._authHttp.put(`${this._API_TYPE}/${info.id}`, body, options)
      .map(res => <Permission>res.json())
      .catch(this.handleError);
  }
  
  

  private handleError(error: Response) {
    // in a real world app, we may send the error to some remote logging infrastructure
    // instead of just logging it to the console
    // console.error(error);
    return Observable.throw(error.json() || 'Server error');
  }

}
