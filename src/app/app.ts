/*
 * Angular 2 decorators and services
 */
import {Component} from 'angular2/core';
import {RouteConfig, Router} from 'angular2/router';

import {Home} from './home/home';
import {HeaderComponent} from './header/header';
import {Admin} from './admin/admin';
import {AddCredit} from './addCredit/addCredit';
import {Profile} from './user/profile/profile';
import {History} from './user/history/history';
import {LoggedInRouterOutlet} from './shared/directives/router';
import {NotLogged} from './notLogged/notLogged';
import {Tournament} from './tournament/tournament';
import {Challenge} from './challenge/challenge';
import {News} from './news/news';
import {UserCPM} from './user/user/user';
import {Mailbox} from './user/mailbox/mailbox';
import {CTV} from './user/ctv/ctv';
import {Gift} from './gift/gift';
import {Order} from './user/order/order';
import {VLTD} from './user/vltk/vltk';
import {CONFIG} from './config';
import {SocketService} from './shared/services/socket';
declare var io: any;

/*
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'app',
  directives: [HeaderComponent, LoggedInRouterOutlet],
  providers: [SocketService],
  pipes: [],
  styles: [`
    main {
      font-size: 1rem;
      line-height: 1.5;
      color: #666;
    }
    :global(body) {
      color: #666;
      background-color: #ededed;
    }
    .notifSys {
      width: 100%;
      background: rgba(0, 0, 0, 0.9);
      height: 100%;
      z-index: 999;
      display: flex;
      justify-content: center;
      align-items: center;
      -webkit-box-orient: vertical;
      -webkit-box-direction: normal;
      -webkit-box-pack: center;
      -webkit-box-align: center;
      position: fixed;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      flex-direction: column;
    }

  `],
  template: `
    <header-component></header-component>
    <main>
      <div class="notifSys" *ngIf="notif"><h3 class="text-danger">{{notif}}</h3></div>
      <auth-router-outlet></auth-router-outlet>
    </main>
  `
})
@RouteConfig([
  { path: '/', component: Home, name: 'Index' },
  { path: '/home', component: Home, name: 'Home' },
  { path: '/doi-qua', component: Gift, name: 'Gift' },
  { path: '/giai-dau/...', component: Tournament, name: 'Tournament' },
  { path: '/mini-game/...', component: Challenge, name: 'Challenge' },
  { path: '/tin-tuc/:id', component: News, name: 'News' },

  // User Control panel
  { path: '/user', component: Profile, name: 'Profile' },
  { path: '/user/hoi-vien', component: UserCPM, name: 'UserCPM' },
  { path: '/user/nap-xu', component: AddCredit, name: 'AddCredit' },
  { path: '/user/lich-su', component: History, name: 'History' },
  { path: '/user/ctv', component: CTV, name: 'CTV' },
  { path: '/user/mailbox', component: Mailbox, name: 'Mailbox' },
  { path: '/user/don-hang', component: Order, name: 'Order' },
  // { path: '/user/vltd', component: VLTD, name: 'VLTD' },
  // Admin control panel
  { path: '/admin/...', component: Admin, name: 'Admin' },
  { path: '/not-logged', component: NotLogged, name: 'NotLogged' },

  // { path: '/**', redirectTo: ['Index'] }
])

export class App {
  notif: any;
  constructor(private _router: Router, private _SS: SocketService) {
    if (this._router.hostComponent !== 'TournamentHome' || this._router.hostComponent !== 'TournamentDetail') {
      (<any>$('body')).css('background-image', 'url("/assets/img/game/bg-d.jpg")');
    }

    this._SS.getnotifSys().subscribe(
      res => {
        this.notif = res;
      }
      , err => console.log(err)
      );
  }

}
