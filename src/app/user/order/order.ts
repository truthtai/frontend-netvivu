import {Component} from 'angular2/core';
import {GiftService} from '../../shared/services/gift';
import {OrderService} from '../../shared/services/order';
import { SweetAlertService } from '../../shared/services/sweet-alert';
import {DateFormatPipe} from 'angular2-moment';

@Component({
  selector: 'user-order',
  template: require('./order.html'),
  styles: [require('./order.scss')],
  providers: [GiftService, SweetAlertService, OrderService],
  pipes: [DateFormatPipe]
})

export class Order {
  Orders: Array<any>;
  selectedOrder: any = {};
  noteOrder: any;
  statusOrder: any;
  constructor(
    // public _US: UserService,
    public _GS: GiftService,
    public _OS: OrderService,
    public _notif: SweetAlertService
    ) {
    this.getAllOrder();
  }

  getAllOrder() {
    this._OS.getAll().subscribe(res => { this.Orders = <any>res; }, error => { });
  }

  _selectedOrder(order: any) {
    (<any>$('#orderReview')).modal('show');
    this.selectedOrder = order;
  }
}
