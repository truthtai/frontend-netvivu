import {Component} from 'angular2/core';
import {CanActivate, Router} from 'angular2/router';
import {UserService} from '../../shared/services/user';
import {JwtHelper} from 'angular2-jwt';
import {NgClass, NgIf, FormBuilder, Validators} from 'angular2/common';
import { ValidationService} from '../../shared/services/validation';
import { SweetAlertService } from '../../shared/services/sweet-alert';
import {NavHeader} from '../../navHeader/navHeader';
// import { AddressService } from '../../shared/services/address';
import {DateFormatPipe} from 'angular2-moment';

@Component({
  selector: 'user-profile',
  template: require('./profile.html'),
  styles: [require('./profile.scss')],
  providers: [UserService, SweetAlertService, JwtHelper],
  directives: [NgIf, NavHeader],
  pipes: [DateFormatPipe]
})

// @CanActivate(() => { return UserService.checkLogin() })
// @RouteConfig([
//   { path: '/admin', component: Home, as: 'Home' }
// ])


export class Profile {
  profileForm: any;
  profile: any = {};
  // cities: any;
  // districts: any;
  constructor(
    // public _US: UserService,
    public _UserService: UserService,
    private _fb: FormBuilder,
    public _notif: SweetAlertService,
    public router: Router
  // public _address: AddressService
    ) {
    this.profile.extraInfo = {};
    // this._address.getAllCity().subscribe(res => this.cities = res, err => console.log(err));
    this.profileForm = this._fb.group({
      'username': [''],
      'password': [''],
      'email': [''],
      'phone': [''],
      'name': [''],
      'businessName': [''],
      'address': [''],
      'bank': [''],
      'gpkd': [''],
      'district': [''],
      'city': [''],
    });
  }

  ngOnInit() {
    this._UserService.getProfile().subscribe((res) => this.profile = res, (error) => { this._notif.swal(`Có lỗi xảy ra: ${error.message}`, 'error'); });
  }
  // _chooseCity(id: any) {
  //   this._address.getAllDistrict(id).subscribe(res => this.districts = res, err => console.log(err));
  // }
  doAddCredit() {
    // if (!this.addCreditForm.issuer || !this.addCreditForm.cardSerial || !this.addCreditForm.cardCode) {
    //   this._notif.swal('Xin vui lòng điền đầy đủ thông tin!', 'error');
    // } else {
    //   this._UserService.addCredit(this.addCreditForm.value).subscribe(res => {
    //     this._UserService._add_token_key(res);
    //     this._notif.swal('Bạn đã nạp card thành công. XU sẽ được cập nhật ngay lập tức. Hệ thống sẽ tự động chuyển trang', 'success', 5000, false);
    //     setTimeout(() => { this.router.navigate(['<AddCredit>']); }, 5000);
    //   }, (error) => { this._notif.swal(`Có lỗi xảy ra: ${error.message}`, 'error'); });
    // };
  }

}
