import {Component} from 'angular2/core';
import {CanActivate, Router} from 'angular2/router';
import {UserService} from '../../shared/services/user';
import {MailboxService} from '../../shared/services/mailbox';
import {SocketService} from '../../shared/services/socket';
import {NgClass, NgIf, NgSwitch, FormBuilder, Validators} from 'angular2/common';
import {NavHeader} from '../../navHeader/navHeader';
import { SweetAlertService } from '../../shared/services/sweet-alert';
import {DateFormatPipe} from 'angular2-moment';
type IMailbox = Mailbox[];

@Component({
  selector: 'user-mailbox',
  template: require('./mailbox.html'),
  styles: [require('./mailbox.scss')],
  providers: [UserService, MailboxService, SocketService, SweetAlertService],
  directives: [NgIf, NgSwitch, NavHeader],
  pipes: [DateFormatPipe]
})

export class Mailbox {
  histories: any;
  inbox: any = [];
  inboxUnread: any = [];
  outbox: any = [];
  userData: any = {};
  mailBoxForm: any;
  Users: any = [];
  selectedUser: any = {};
  selectedMail: any = {};
  mail: any = {};
  submitting = false;
  credit = 0;
  haveCredit = false;
  constructor(
    public _US: UserService,
    public router: Router,
    public _MB: MailboxService,
    private _fb: FormBuilder,
    public _SS: SocketService,
    public _notif: SweetAlertService
    ) {

    this.getInbox();
    this.getOutbox();
    this.mailBoxForm = this._fb.group({
      'title': ['', Validators.required],
      'description': ['', Validators.required],
      'receiver': ['', Validators.required]
    });

  }

  ngOnInit() {
    this.userData = this._US.extract();
    this.getInbox$();
    this.getOutbox$();
  }

  search(name: any) {
    this._US.searchUser(name)
      .subscribe(
      res => {
        this.Users = res;
      }
      , error => {
        this._notif.swal('Có lỗi xảy ra: ' + error.mesaage, 'error');
      }
      );
  }

  getInbox$() {
    this._SS.getInbox()
      .subscribe((res: IMailbox[]) => {
      this.inbox = res;
      this.inboxUnread = res.filter((mail: any) => {
        return mail.receiver.action.read === false;
      });
    }, (error) => { });
  }

  getInbox() {
    this._MB.getInbox()
      .subscribe((res: IMailbox[]) => {
      this.inbox = res;
      this.inboxUnread = res.filter((mail: any) => {
        return mail.receiver.action.read === false;
      });
    }, (error) => { });
  }

  getOutbox() {
    this._MB.getOutbox()
      .subscribe((res) => {
      this.outbox = res;
    }, (error) => { });
  }

  getOutbox$() {
    this._SS.getOutbox()
      .subscribe((res: IMailbox[]) => {
      this.outbox = res;
    }, (error) => { });
  }

  send() {

  }

  delete(id: any) {
    this._MB.delete(id)
      .subscribe(res => {
      this._notif.swal('Xóa thành công thư', 'success');
    }, (error) => {
        this._notif.swal('Có lỗi xảy ra: ' + error.mesaage, 'error');
      });
  }

  doSubmit() {
    if (this.mailBoxForm.dirty && this.mailBoxForm.valid) {
      if (!this.selectedUser._id) {
        this._notif.swal('Vui lòng chọn 1 user trong danh sách', 'error');
      } else {

        this.mailBoxForm.value.receiver = this.selectedUser._id;
        if (this.haveCredit && this.credit > 0) {
          this.mailBoxForm.value.credit = this.credit;
        };

        this._notif.swalConfirm('Gửi thư', 'Bạn có muốn gửi thư này không?', (result) => {
          if (result) {
            this._MB.send(this.mailBoxForm.value)
              .subscribe(res => {
              this._notif.swal('Gửi thành công', 'success');
              this._reset();
            }, error => {
                this._notif.swal('Có lỗi xảy ra: ' + error.message, 'error');
              });
          } else {

          }
        });

      }

    }
  }

  _change(value) {
    this.search(value.target.value);
  }

  _selectedUser(value) {
    this.selectedUser = value;
  }

  _reset() {
    this.mail = {};
    this.Users = [];
    this.selectedUser = '';
  }

  _view(id: any) {
    (<any>$('#mailboxReview')).modal('show');
    this._MB.get(id)
      .subscribe((res: any) => {
      this.selectedMail = res;
      this.selectedMail.description = res.description.replace(/\n/g, '<br>');
      this.getInbox();
    }, error => {
        this._notif.swal('Có lỗi xảy ra: ' + error.mesaage, 'error');
      });
  }

  _delete(id: any) {
    this._notif.swalConfirm('Xóa thư', 'Bạn có muốn xóa thư này không?', (result) => {
      if (result) {
        this._MB.delete(id)
          .subscribe(res => {
          this._notif.swal('Xóa thư thành công', 'success');
          this.getInbox();
        }, error => {
            this._notif.swal('Có lỗi xảy ra ' + error, 'error');
          });
      }
    });
  }

  _receiveCredit() {
    this._notif.swalConfirm('Giao dịch', 'Bạn có muốn nhận giao dịch này không?', (result) => {
      if (result) {
        this._MB.receiveCredit(this.selectedMail._id)
          .subscribe(res => {
          this._notif.swal('Giao dịch thành công', 'success');
          this.getInbox();
          (<any>$('#mailboxReview')).modal('hide');
        }, error => {
            this._notif.swal('Có lỗi xảy ra ' + error, 'error');
            (<any>$('#mailboxReview')).modal('hide');
          });
      }
    });
  }

}
