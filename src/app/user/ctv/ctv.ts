import {Component} from 'angular2/core';
import {CanActivate, Router} from 'angular2/router';
import {JwtHelper} from 'angular2-jwt';
import {UserService} from '../../shared/services/user';
import {NgClass, NgIf, NgSwitch, FormBuilder, Validators} from 'angular2/common';
import {DateFormatPipe} from 'angular2-moment';
import { AddressService } from '../../shared/services/address';
import {NavHeader} from '../../navHeader/navHeader';
import { SweetAlertService } from '../../shared/services/sweet-alert';

@Component({
  selector: 'user-ctv',
  template: require('./ctv.html'),
  styles: [require('./ctv.scss')],
  providers: [UserService, SweetAlertService, JwtHelper, AddressService],
  directives: [NgIf, NgSwitch, NavHeader],
  pipes: [DateFormatPipe]
})

export class CTV {
  Users: any = [];
  userData: any;
  upgradeCPMbyCTVForm: any;
  districts: any;
  cities: any;
  _userSelected: any;
  constructor(
    private _fb: FormBuilder,
    public _UserService: UserService,
    public router: Router,
    public _notif: SweetAlertService,
    public _jwt: JwtHelper,
    public _address: AddressService
    ) {
    this._address.getAllCity().subscribe(res => this.cities = res, err => console.log(err));
    this.upgradeCPMbyCTVForm = this._fb.group({
      'businessName': ['', Validators.required],
      'address': ['', Validators.required],
      'bank': ['', Validators.required],
      'gpkd': ['', Validators.required],
      'district': ['', Validators.required],
      'city': ['', Validators.required],
    });

    this._getAllUser();
  }


  ngOnInit() {
    var token = localStorage.getItem('auth_token');
    if (token) {
      this.userData = this._jwt.decodeToken(token);
    }
  }

  _getAllUser() {
    this._UserService.getAllUserByUser().subscribe(res => { this.Users = <any>res; }, error => { });
  }

  searchUser(value: any) {
    this._UserService.getAllMember(value).subscribe(res => { this.Users = <any>res; }, error => { });
  }

  onChange(value: any) {
    this._UserService.getAllMember(value).subscribe(res => { this.Users = <any>res; }, error => { });
  }

  doUpgradeCPM_1() {
    this._notif.swalConfirm('Nâng cấp tài khoản', 'Bạn có muốn nâng cấp tài khoản này không?', (result) => {
      if (result) {
        this._UserService.upgradeCPMByCTV(this._userSelected, this.upgradeCPMbyCTVForm.value)
          .subscribe(res => {
          this._getAllUser();
          this._notif.swal(`Bạn đã nâng cấp thành công. Hệ thống sẽ cập nhật ngay lập tức`, 'success');
          (<any>$('#upgradeCPMbyCTV')).modal('hide');
        }
          , err => this._notif.swal(`Lỗi: ${err}`, 'error'));
      }
    });
  }

  doUpgrade(id: any) {
    if (this.userData.credit >= 400) {
      (<any>$('#upgradeCPMbyCTV')).modal('show');
      this._userSelected = id;
      // this.doUpgradeCPM_1(id);
    } else {
      this._notif.swal('Bạn không đủ xu để nâng cấp. Vui lòng nạp thêm xu', 'error');
    }
  }

  _chooseCity(id: any) {
    this._address.getAllDistrict(id).subscribe(res => this.districts = res, err => console.log(err));
  }

  doExtend(id: any) {
    if (this.userData.credit >= 400) {
      this._notif.swalConfirm('Gia hạn tài khoản', 'Bạn có muốn gia hạn tài khoản này không?', (result) => {
        if (result) {
          this._UserService.extendCPMByCTV(id)
            .subscribe(res => {
            this._getAllUser();
            this._notif.swal(`Bạn đã gia hạn thành công. Hệ thống sẽ cập nhật ngay lập tức`, 'success');
          }
            , err => this._notif.swal(`Lỗi: ${err}`, 'error'));
        }
      });
    } else {
      this._notif.swal('Bạn không đủ xu để gia hạn. Vui lòng nạp thêm xu', 'error');
    }

  }

}
