import {Component} from 'angular2/core';
import {CanActivate, Router} from 'angular2/router';
import {UserService} from '../../shared/services/user';
import {NgClass, NgIf, NgSwitch} from 'angular2/common';
import {JwtHelper} from 'angular2-jwt';
import {NavHeader} from '../../navHeader/navHeader';
// import { AddressService } from '../../shared/services/address';
//import _ from 'lodash';
import {DateFormatPipe} from 'angular2-moment';
type IHistory = History[];

@Component({
  selector: 'user-history',
  template: require('./history.html'),
  styles: [require('./history.scss')],
  providers: [UserService, JwtHelper],
  directives: [NgIf, NgSwitch, NavHeader],
  pipes: [DateFormatPipe]
})

export class History {
  histories: any;
  constructor(
    public _UserService: UserService,
    public router: Router
    ) {


  }

  ngOnInit() {
    this._UserService.getHistory()
      .subscribe((res: IHistory[]) => {
      this.histories = res.map((history: any) => {
        return history;
      });
    }, (error) => { });
  }

}
