import {Component} from 'angular2/core';
import {CanActivate, Router} from 'angular2/router';
import {UserService} from '../../shared/services/user';
import {NgClass, NgIf, NgSwitch} from 'angular2/common';
import {NavHeader} from '../../navHeader/navHeader';
import { SweetAlertService } from '../../shared/services/sweet-alert';
// import { AddressService } from '../../shared/services/address';
//import _ from 'lodash';
import { SettingService } from '../../shared/services/setting';
import {SocketService} from '../../shared/services/socket';
import {DateFormatPipe} from 'angular2-moment';
type IHistory = History[];

@Component({
  selector: 'user-vltk',
  template: require('./vltk.html'),
  styles: [require('./vltk.scss')],
  providers: [UserService, SweetAlertService, SocketService, SettingService],
  directives: [NgIf, NgSwitch, NavHeader],
  pipes: [DateFormatPipe]
})

export class VLTD {
  userData: any;
  isChecking: any = false;
  JX: any;
  constructor(
    public _US: UserService,
    public router: Router,
    public _notif: SweetAlertService,
    public _SS: SocketService,
    public _S: SettingService
    ) {
    this.userData = this._US.extract();
    this._SS.getInfo().subscribe(
      res => {
        this.userData = res;
      }
      , err => { console.log(err); }
      );
    this.getJx();
  }
  ngOnInit() {
    this._S.setBg('vltk');

    if (!this._US.getTypeUser() || (this._US.getTypeUser() !== 'admin' && this._US.getTypeUser() !== 'HV' && this._US.getTypeUser() !== 'member')) {
      this.router.navigateByUrl('/not-logged');
    }
  }

  getJx() {
    this._US.getJx()
      .subscribe(res => {
      this.isChecking = false;
      this.JX = res;
    }, (error) => { });
  }

  joinJx(password: any) {
    if (password) {
      this._notif.swalConfirm('Tạo tài khoản', 'Bạn có muốn tạo tài khoản này không?', (result) => {
        if (result) {
          this._US.joinJx({ password })
            .subscribe(
            res => {
              this._notif.swal(`Tạo Thành công`, 'success');
              this.getJx();
              this.JX = res;
            }, (error) => { this._notif.swal(`Có lỗi xảy ra: ${error.message}`, 'error'); });
        }
      });
    }
  }

  tradeKnbJx(knb: any) {
    if (knb) {
      this._notif.swalConfirm('Đổi Kim nguyên bảo', 'Bạn có muốn đổi Kim nguyên bảo không?', (result) => {
        if (result) {
          this._US.tradeKnbJx({ knb })
            .subscribe(
            res => {
              this._notif.swal(`Đổi Kim nguyên bảo thành công`, 'success');
              this.getJx();
            }, (error) => { this._notif.swal(`Có lỗi xảy ra: ${error.message}`, 'error'); });
        }
      });
    }
  }

}
