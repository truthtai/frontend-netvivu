import {Component} from 'angular2/core';
import {CanActivate, Router} from 'angular2/router';
import {UserService} from '../../shared/services/user';
import {NgClass, NgIf, NgSwitch} from 'angular2/common';
import {JwtHelper} from 'angular2-jwt';
import {NavHeader} from '../../navHeader/navHeader';
import { SweetAlertService } from '../../shared/services/sweet-alert';
// import { AddressService } from '../../shared/services/address';
//import _ from 'lodash';
import {DateFormatPipe} from 'angular2-moment';
type IHistory = History[];

@Component({
  selector: 'user-cpm',
  template: require('./user.html'),
  styles: [require('./user.scss')],
  providers: [UserService, JwtHelper, SweetAlertService],
  directives: [NgIf, NgSwitch, NavHeader],
  pipes: [DateFormatPipe]
})

export class UserCPM {
  users: any;
  constructor(
    public _UserService: UserService,
    public router: Router,
    public _notif: SweetAlertService
    ) {
  }

  ngOnInit() {
    this.getAllUser();
  }

  getAllUser() {
    this._UserService.getAllUserByUser()
      .subscribe((res: IHistory[]) => {
      this.users = res;
    }, (error) => { });
  }

  deleteUserByOwner(id: any) {
    this._notif.swalConfirm('Xóa tài khoản', 'Bạn có muốn xóa tài khoản này không?', (result) => {
      if (result) {
        this._UserService.deleteUserByOwner(id)
          .subscribe(
          res => {
            this._notif.swal(`Xóa User Thành công`, 'success');
            this.getAllUser();
          }, (error) => { this._notif.swal(`Có lỗi xảy ra: ${error.message}`, 'error'); });
      }
    });
  }

}
