import {Component} from 'angular2/core';
import {UserService} from '../shared/services/user';
import {RouterActive} from '../directives/router-active';
import {SocketService} from '../shared/services/socket';
import { SweetAlertService } from '../shared/services/sweet-alert';

@Component({
  selector: 'nav-header',
  template: require('./navHeader.html'),
  styles: [require('./navHeader.scss')],
  providers: [UserService, SocketService, SweetAlertService],
  directives: [RouterActive]
})

export class NavHeader {
  userData: any = {};
  constructor(
    private _SS: SocketService,
    private _NF: SweetAlertService,
    private _US: UserService
    ) {
  }

  ngOnInit() {
    this.userData = this._US.extract();
    this._SS.getInfo().subscribe(data => this.userData = data);
    this._SS.getNotif().subscribe(data => {
      this._NF.swal(data, 'warning');
    });
  }

}
