import {Component} from 'angular2/core';

@Component({
  selector: 'slider-component',
  template: require('./slider.html'),
  styles: [require('./slider.scss')]
})

export class SliderComponent {
  constructor() {

  }

  ngOnInit() {
    // console.log('hello `header` component');
  }

}
