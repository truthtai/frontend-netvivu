import {Component} from 'angular2/core';
import {NavHeader} from '../navHeader/navHeader';
import {SliderComponent} from '../slider/slider';
import { GiftService } from '../shared/services/gift';
import { OrderService } from '../shared/services/order';
import {CONFIG} from '../config';
import * as _ from 'lodash';
import {JwtHelper} from 'angular2-jwt';
import { SweetAlertService } from '../shared/services/sweet-alert';

@Component({
  selector: 'gift',
  template: require('./gift.html'),
  styles: [require('./gift.scss')],
  directives: [NavHeader, SliderComponent],
  providers: [GiftService, JwtHelper, SweetAlertService, OrderService],
})

export class Gift {
  Gifts: any = [];
  CPGifts: any = [];
  CGGifts: any = [];
  KGifts: any = [];
  addedToCart: any = [];
  total: any = 0;
  URL_IMG: any;
  userData: any;
  constructor(public _GS: GiftService, public _jwt: JwtHelper, public _notif: SweetAlertService, public _OS: OrderService) {
    this.userData = this._jwt.decodeToken(localStorage.getItem('auth_token'));
  }

  ngOnInit() {
    this.Gifts = [
      { cat: 'CARD_PHONE', v: [], name: 'Thẻ điện thoại' },
      { cat: 'CARD_GAME', v: [], name: 'Thẻ Game' },
      { cat: 'KHAC', v: [], name: 'Khác' }
    ];

    this.URL_IMG = `${CONFIG.API_URL}/image`;
    this._GS.getS('CARD_PHONE').subscribe(
      res => {
        this.Gifts[0].v = res;
        this.CPGifts = res;
      }
      , err => console.log(err)
      );
    this._GS.getS('CARD_GAME').subscribe(
      res => {
      this.Gifts[1].v = res;
      this.CGGifts = res;
      }
      , err => console.log(err)
      );
    this._GS.getS('KHAC').subscribe(
      res => {
        this.Gifts[2].v = res;
        this.KGifts = res;
      }
      , err => console.log(err)
      );
    // console.log('hello `header` component');
  }

  _addToCart(gift: any, quantity: any) {
    gift.quantity = parseInt(quantity);
    gift.total = gift.quantity * gift.credit;
    let leftCredit = this.userData.credit - this.total;
    if (this.userData.credit > this.total) {
      const index = _.findIndex(this.addedToCart, _.pick(gift, '_id'));
      if (index !== -1) {
        if (gift.quantity === 0) {
          this.addedToCart.splice(index, 1);
        } else {
          this.addedToCart.splice(index, 1, gift);
        }
      } else {
        this.addedToCart.push(gift);
      }
      this._total();
    } else {
      this._total();
      this._notif.swal(`Không đủ xu để thực hiện! Xu còn: ${leftCredit}. Xu cần: ${gift.total}`, 'error');
    }
  }

  _total() {
    this.total = _.reduce(this.addedToCart, (total, product: any) => {
      return total + parseInt(product.total);
    }, 0);
  }

  _leftCredit() {
    return this.userData.credit - this.total;
  }

  doConfirm() {
    (<any>$('#confirmOrder')).modal('show');
  }

  doSubmit() {
    (<any>$('#confirmOrder')).modal('hide');
    this._notif.swalConfirm('Đổi vật phẩm', 'Bạn có muốn đổi vật phẩm không?', (result) => {
      if (result) {
        this._OS.add({order: this.addedToCart})
          .subscribe(res => {
          this.addedToCart = [];
          this.total = 0;
          this._notif.swal(`Đổi thành công. <p><strong>Vui lòng đợi xác nhận từ Quản trị trong vòng 24 giờ</strong></p>
            <p><strong class="text-danger">Khi có kết quả Hệ thống sẽ thông báo qua SDT và hộp thư của bạn</strong></p>`, 'success');
        }, error => {
            this._notif.swal('Có lỗi xảy ra ' + error.message, 'error');
          });
      }
    });
  }

}
