import {Pipe} from 'angular2/core';
// declare var moment: any;
import * as moment from 'moment';

@Pipe({
  name: 'dateFormat'
})
export class DateFormat {

  constructor() {
  }

  transform(date, args) {
    return date.replace('Z', '');
  }
}
