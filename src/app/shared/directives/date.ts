import {Component, Input, Output, EventEmitter, OnInit, Directive, HostBinding, HostListener} from 'angular2/core';
import {NgFor, NgModel} from 'angular2/common';
@Directive({
  selector: 'input[type=datetime-local]',
  // host: {
  //   //   '[value]': '_date',
  //   //   '(change)': 'onDateChange($event.target.value)'
  // }
})

// @HostBindings({
//   '[value]': '_date',
//   '(change)': 'onDateChange($event.target.value)'
// })
// @HostBinding('[value]') { return '[_date]' }

export class SelectDateTime {
  public _date: string;
  @Input() set date(d: Date) {
    this._date = this.toDateString(d);
  }
  @Output() dateChange: EventEmitter<Date>;

  // @HostBinding('value')
  // get _date {
  //     return '_date';
  // }
  @HostBinding('value') value = this._date;

  @HostListener('change', [
    '$event.target.value'
  ])
  onChange: Function = (_) => { this.onDateChange(_); };

  constructor() {
    this.date = new Date();
    this.dateChange = new EventEmitter();
  }

  public toDateString(date: Date): string {
    return (date.getFullYear().toString() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + (date.getDate())).slice(-2))
      + 'T' + date.toTimeString().slice(0, 5);
  }
  public parseDateString(date: string): Date {
    date = date.replace('T', '-');
    var parts: any = date.split('-');
    var timeParts = parts[3].split(':');

    // new Date(year, month [, day [, hours[, minutes[, seconds[, ms]]]]])
    return new Date(parts[0], parts[1] - 1, parts[2], timeParts[0], timeParts[1]); // Note: months are 0-based
  }

  public onDateChange(value: string): void {
    if (value !== this._date) {
      var parsedDate = this.parseDateString(value);

      // check if date is valid first
      if (parsedDate.getTime() !== NaN) {
        this._date = value;
        this.dateChange.emit(parsedDate);
      }
    }
  }
}
