import {Directive, Attribute, ElementRef, DynamicComponentLoader} from 'angular2/core';
import {Router, RouterOutlet, ComponentInstruction} from 'angular2/router';
// import {Login} from '../login/login';
import {UserService} from '../services/user';
import {JwtHelper} from 'angular2-jwt';

@Directive({
  selector: 'auth-router-outlet',
  providers: [UserService, JwtHelper]
})
export class LoggedInRouterOutlet extends RouterOutlet {
  publicRoutes: any;
  adminRoutes: any;
  isAdmin: any;
  private parentRouter: Router;
  constructor(_elementRef: ElementRef,
    _loader: DynamicComponentLoader,
    _parentRouter: Router,
    @Attribute('name') nameAttr: string,
    private _userS: UserService
    ) {
    super(_elementRef, _loader,  _parentRouter, nameAttr);

    this.parentRouter = _parentRouter;
    this.isAdmin = this._userS.extract().type !== 'admin';
    this.publicRoutes = {
      '/login': true,
      '/signup': true,
      '/?': true,
      '/home': true,
      '': true
    };
    this.adminRoutes = {
      '/admin': true,
      '/admin/user': true,
      '/admin/tournament': true,
      '/admin/tournament/create': true,
      '/admin/tournament/edit': true
    };

  }

  activate(instruction: ComponentInstruction) {
    if (this.parentRouter.hostComponent !== 'TournamentHome' || this.parentRouter.hostComponent !== 'TournamentDetail') {
      (<any>$('body')).css('background-image', 'url("/assets/img/game/bg-d.jpg")');
    }
    var url = this.parentRouter.lastNavigationAttempt;
    if (!this.publicRoutes[url] && !localStorage.getItem('auth_token')) {
      // todo: redirect to Login, may be there a better way?
      this.parentRouter.navigateByUrl('/not-logged');
    }
    if (this.adminRoutes[url] && localStorage.getItem('auth_token') && this.isAdmin) {
      alert('Bậy rồi nha !!!');
    }
    return super.activate(instruction);
  }
}
//
//
// class Authenticated implements BeforeActivate {
//     constructor(public router: Router, public _US: UserService) {}
//
//     routerBeforeActivate(next: ComponentInstruction, prev: ComponentInstruction) {
//         if (this._US.getTypeUser() && this._US.getTypeUser() === 'admin') {
//             this.router.navigate(['/not-logged']);
//             return false;
//         }
//
//         return true; // or return promise that resolves to true
//     }
// }
