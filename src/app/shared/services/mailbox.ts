import {Injectable} from 'angular2/core';
import {Http, Response} from 'angular2/http';
import {Observable}     from 'rxjs/Observable';
import {Headers, RequestOptions} from 'angular2/http';
import {AuthHttp, JwtHelper} from 'angular2-jwt';
import {CONFIG} from '../../config';

declare var window;
interface Mailbox {
  // accessToken: string;
}


@Injectable()

export class MailboxService {

  constructor(public http: Http, public _authHttp: AuthHttp, public _jwt: JwtHelper) {

  }

  private _API_ = `${CONFIG.API_URL}/mailbox`;


  getInbox(): Observable<Mailbox> {
    return this._authHttp.get(`${this._API_}/inbox`)
      .map(res => <Mailbox>res.json())
      .catch(this.handleError);
  }

  getInboxUnread(): Observable<Mailbox> {
    return this._authHttp.get(`${this._API_}/unread`)
      .map(res => <Mailbox>res.json())
      .catch(this.handleError);
  }

  getOutbox(): Observable<Mailbox> {
    return this._authHttp.get(`${this._API_}/outbox`)
      .map(res => <Mailbox>res.json())
      .catch(this.handleError);
  }

  get(id: any): Observable<Mailbox> {
    return this._authHttp.get(`${this._API_}/${id}`)
      .map(res => <Mailbox>res.json())
      .catch(this.handleError);
  }

  delete(id: any): Observable<Mailbox> {
    return this._authHttp.delete(`${this._API_}/${id}`)
      .map(res => <Mailbox>res.json())
      .catch(this.handleError);
  }

  send(info: any): Observable<Mailbox> {
    let body = JSON.stringify(info);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this._authHttp.post(this._API_, body, options)
      .map(res => <Mailbox>res.json().message)
      .catch(this.handleError);
  }

  receiveCredit(id: any): Observable<Mailbox> {
    return this._authHttp.get(`${this._API_}/${id}/receive`)
      .map(res => <Mailbox>res.json())
      .catch(this.handleError);
  }

  private handleError(error: Response) {
    // in a real world app, we may send the error to some remote logging infrastructure
    // instead of just logging it to the console
    // console.error(error);
    return Observable.throw(error.json() || 'Server error');
  }

}
