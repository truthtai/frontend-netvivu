import {Injectable} from 'angular2/core';
import {Http, Response} from 'angular2/http';
import {Observable}     from 'rxjs/Observable';
import {Headers, RequestOptions} from 'angular2/http';
import {AuthHttp, JwtHelper} from 'angular2-jwt';
import {CONFIG} from '../../config';

declare var window;
interface Tournament {
  // accessToken: string;
}


@Injectable()

export class TournamentService {

  constructor(public http: Http, public _authHttp: AuthHttp, public _jwt: JwtHelper) {

  }

  private _API_TOURNAMENT = `${CONFIG.API_URL}/tournament`;


  getAllTournament(): Observable<Tournament> {

    return this._authHttp.get(this._API_TOURNAMENT)
      .map(res => <Tournament>res.json())
      .catch(this.handleError);
  }


  getTournament(id: any): Observable<Tournament> {
    return this._authHttp.get(`${this._API_TOURNAMENT}/${id}`)
      .map(res => <Tournament>res.json())
      .catch(this.handleError);
  }

  deleteTournament(id: any): Observable<Tournament> {
    return this._authHttp.delete(`${this._API_TOURNAMENT}/${id}`)
      .map(res => <Tournament>res.json())
      .catch(this.handleError);
  }

  addTournament(info: any) {
    return new Promise((resolve, reject) => {
      var formData: any = new FormData();
      var xhr = new XMLHttpRequest();
      if (info.image) {
        formData.append('file', info.image);
      }

      formData.append('name', info.name);
      formData.append('description', info.description);
      formData.append('startReg', info.startReg);
      formData.append('endReg', info.endReg);
      formData.append('startRun', info.startRun);
      formData.append('endRun', info.endRun);
      formData.append('freeFirstTime', info.freeFirstTime);
      formData.append('remindTime', info.remindTime);
      formData.append('randomTime', info.randomTime);
      formData.append('game', info.game);
      formData.append('credit', info.credit);

      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      };
      xhr.open('POST', this._API_TOURNAMENT, true);
      xhr.setRequestHeader('authorization', 'Bearer ' + localStorage.getItem('auth_token'));
      xhr.send(formData);
    });
  }

  editTournament(info: any, id: any): Observable<Tournament> {
    let body = JSON.stringify(info);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this._authHttp.put(`${this._API_TOURNAMENT}/${id}`, body, options)
      .map(res => <Tournament>res.json().message)
      .catch(this.handleError);
  }

  join(info: any, id: any): Observable<Tournament> {
    info['id'] = id;
    let body = JSON.stringify(info);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this._authHttp.post(`${this._API_TOURNAMENT}/join`, body, options)
      .map(res => <Tournament>res.json())
      .catch(this.handleError);
  }

  exportTournament(id: any) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', `${this._API_TOURNAMENT}/export/${id}`, true);
    xhr.setRequestHeader('authorization', 'Bearer ' + localStorage.getItem('auth_token'));
    xhr.responseType = 'arraybuffer';

    xhr.onload = function(oEvent) {
      var arrayBuffer = xhr.response; // Note: not oReq.responseText
      if (arrayBuffer) {
        var blob = new Blob([arrayBuffer], { type: 'application/vnd.openxmlformats' });
        var fileName = 'Danh-sach-cac-doi-tham-gia.xlsx';
        var url = window.URL.createObjectURL(blob);
        var a: any = document.createElement('a');
        document.body.appendChild(a);
        a.style = 'display: none';
        a.href = url;
        a.download = fileName;
        a.click();
        window.URL.revokeObjectURL(url);
      }
    };

    xhr.send(null);
  }

  private handleError(error: Response) {
    // in a real world app, we may send the error to some remote logging infrastructure
    // instead of just logging it to the console
    // console.error(error);
    return Observable.throw(error.json() || 'Server error');
  }

}
