import {Injectable} from 'angular2/core';
import {Http, Response} from 'angular2/http';
import {Observable}     from 'rxjs/Observable';
import {Headers, RequestOptions} from 'angular2/http';
import {AuthHttp, JwtHelper} from 'angular2-jwt';
import {CONFIG} from '../../config';

declare var window;
interface Gift {
  // accessToken: string;
}


@Injectable()

export class GiftService {

  constructor(public http: Http, public _authHttp: AuthHttp, public _jwt: JwtHelper) {

  }

  private _API_ = `${CONFIG.API_URL}/gift`;


  getAll(): Observable<Gift> {

    return this._authHttp.get(this._API_)
      .map(res => <Gift>res.json())
      .catch(this.handleError);
  }

  getS(name: any): Observable<Gift> {

    return this._authHttp.get(`${this._API_}/cat/${name}`)
      .map(res => <Gift>res.json())
      .catch(this.handleError);
  }


  get(id: any): Observable<Gift> {
    return this._authHttp.get(`${this._API_}/${id}`)
      .map(res => <Gift>res.json())
      .catch(this.handleError);
  }

  delete(id: any): Observable<Gift> {
    return this._authHttp.delete(`${this._API_}/${id}`)
      .map(res => <Gift>res.json())
      .catch(this.handleError);
  }

  add(info: any) {
    return new Promise((resolve, reject) => {
      var formData: any = new FormData();
      var xhr = new XMLHttpRequest();
      if (info.image) {
        formData.append('file', info.image);
      }

      formData.append('name', info.name);
      formData.append('description', info.description);
      formData.append('credit', info.credit);
      formData.append('price', info.price);
      formData.append('status', info.status);
      formData.append('category', info.category);
      formData.append('subCategory', info.subCategory);

      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      };
      xhr.open('POST', this._API_, true);
      xhr.setRequestHeader('authorization', 'Bearer ' + localStorage.getItem('auth_token'));
      xhr.send(formData);
    });
  }

  edit(info: any, id: any): Observable<Gift> {
    let body = JSON.stringify(info);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this._authHttp.put(`${this._API_}/${id}`, body, options)
      .map(res => <Gift>res.json().message)
      .catch(this.handleError);
  }


  private handleError(error: Response) {
    // in a real world app, we may send the error to some remote logging infrastructure
    // instead of just logging it to the console
    // console.error(error);
    return Observable.throw(error.json() || 'Server error');
  }

}
