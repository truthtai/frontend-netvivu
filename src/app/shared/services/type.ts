import {Injectable} from 'angular2/core';
import {Http, Response} from 'angular2/http';
import {Observable}     from 'rxjs/Observable';
import {Headers, RequestOptions} from 'angular2/http';
import {AuthHttp, JwtHelper} from 'angular2-jwt';
import {CONFIG} from '../../config';

interface Type {
  // accessToken: string;
}

interface AccessToken {
  accessToken: string;
}

interface UserInfo {

}

@Injectable()

export class TypeService {

  constructor(public http: Http, public _authHttp: AuthHttp, public _jwt: JwtHelper) {

  }

  private _API_TYPE = `${CONFIG.API_URL}/type`;
  private _loginUrl = `${this._API_TYPE}/login`;
  private _registerUrl = `${this._API_TYPE}`;

  getAllType(): Observable<Type> {

    return this._authHttp.get(this._API_TYPE)
      .map(res => <Type>res.json())
      .catch(this.handleError);
  }

  editPermissionOfType(info: any): Observable<UserInfo> {
    let body = JSON.stringify(info);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this._authHttp.put(`${this._API_TYPE}/${info.id}`, body, options)
      .map(res => <UserInfo>res.json())
      .catch(this.handleError);
  }

  private handleError(error: Response) {
    // in a real world app, we may send the error to some remote logging infrastructure
    // instead of just logging it to the console
    // console.error(error);
    return Observable.throw(error.json() || 'Server error');
  }

}



