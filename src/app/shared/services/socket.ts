import {Injectable} from 'angular2/core';
import {Http, Response} from 'angular2/http';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import 'rxjs/add/operator/share';
import {Headers, RequestOptions} from 'angular2/http';
import {AuthHttp, JwtHelper} from 'angular2-jwt';
import {CONFIG} from '../../config';
import {EventEmitter} from 'angular2/core';

declare var io: any;

interface Type {
  // accessToken: string;
}

interface AccessToken {
  accessToken: string;
}

interface UserInfo {

}

@Injectable()

export class SocketService {
  userData: any;
  userData$: EventEmitter<UserInfo> = new EventEmitter();
  notif$: EventEmitter<UserInfo> = new EventEmitter();
  msg$: EventEmitter<UserInfo> = new EventEmitter();
  notifChallenge$: EventEmitter<UserInfo> = new EventEmitter();
  config$: EventEmitter<UserInfo> = new EventEmitter();
  team$: EventEmitter<UserInfo> = new EventEmitter();
  countTimer$: EventEmitter<UserInfo> = new EventEmitter();
  isWin$: EventEmitter<UserInfo> = new EventEmitter();
  predictNow$: EventEmitter<UserInfo> = new EventEmitter();
  predictHistory$: EventEmitter<UserInfo> = new EventEmitter();
  predictTimeout$: EventEmitter<UserInfo> = new EventEmitter();
  outbox$: EventEmitter<UserInfo> = new EventEmitter();
  inbox$: EventEmitter<UserInfo> = new EventEmitter();
  inboxUnread$: EventEmitter<UserInfo> = new EventEmitter();
  notifSys$: EventEmitter<UserInfo> = new EventEmitter();
  private socket = io.connect(CONFIG.API_URL, { 'forceNew': true });

  constructor(public http: Http, public _authHttp: AuthHttp, public _jwt: JwtHelper) {
    const token = localStorage.getItem('auth_token');
    if (token) {
      this.userData = this._jwt.decodeToken(token);
      this.socket.on('connect', (s) => {
        this.socket.emit('user', this.userData);
        this.socket.on('userData', (data) => {
          localStorage.setItem('auth_token', data);
          this.userData$.emit(this._jwt.decodeToken(data));
        });

        this.socket.on('notifSysx', (data) => {
          this.notifSys$.emit(data);
        });

        this.socket.on('notif', (data) => {
          this.notif$.emit(data);
        });

        this.socket.on('msg', (data) => {
          this.msg$.emit(data);
        });

        this.socket.on('isTeam', (data) => {
          this.team$.emit(data);
        });

        this.socket.on('predictNow', (data) => {
          this.predictNow$.emit(data);
        });

        this.socket.on('predictHistory', (data) => {
          this.predictHistory$.emit(data);
        });

        this.socket.on('predictTimeout', (data) => {
          this.predictTimeout$.emit(data);
        });

        this.socket.on('countTimer', (data) => {
          this.countTimer$.emit(data);
        });

        this.socket.on('isWin', (data) => {
          this.isWin$.emit(data);
        });

        this.socket.on('config', (data) => {
          this.config$.emit(data);
        });

        this.socket.on('inbox', (data) => {
          this.inbox$.emit(data);
        });

        this.socket.on('outbox', (data) => {
          this.outbox$.emit(data);
        });

        this.socket.on('inboxUnread', (data) => {
          this.inbox$.emit(data);
        });
      });
    }

  }

  connect() {
    this.socket.on('message', (data) => {
      console.log(data);
    });
  }

  getInfo(): Observable<any> {
    return this.userData$;
  }

  getNotif(): Observable<any> {
    return this.notif$;
  }

  getMsg(): Observable<any> {
    return this.msg$;
  }

  getIsWin(): Observable<any> {
    return this.isWin$;
  }

  getConfig(room): Observable<any> {
    this.socket.emit('getConfig', { joinChallenge: room, user: this.userData });
    return this.config$;
  }

  setConfig(room, value: any) {
    let data = {
      joinChallenge: room,
      user: this.userData,
      config: value
    };
    this.socket.emit('setConfig', data);
  }

  joinRoom(room): Observable<any> {
    this.socket.emit('joinChallenge', { joinChallenge: room, user: this.userData });
    return this.notif$;
  }

  confirm(room) {
    let info = {
      joinChallenge: room,
      user: this.userData
    };
    this.socket.emit('confirm', info);
    // return this.notifChallenge$;
  }

  getTeam(room) {
    this.socket.emit('getTeam', { joinChallenge: room, user: this.userData });
    return this.team$;
  }

  getCountTimer() {
    return this.countTimer$;
  }

  getPredictNow() {
    return this.predictNow$;
  }

  getPredictHistory() {
    return this.predictHistory$;
  }

  getPredictTimeout() {
    return this.predictTimeout$;
  }

  getInbox() {
    return this.inbox$;
  }

  getOutbox() {
    return this.outbox$;
  }

  getInboxUnread() {
    return this.inboxUnread$;
  }

  getnotifSys() {
    return this.notifSys$;
  }


  private handleError(error: Response) {
    // in a real world app, we may send the error to some remote logging infrastructure
    // instead of just logging it to the console
    // console.error(error);
    return Observable.throw(error.json() || 'Server error');
  }

}
