import {Injectable} from 'angular2/core';
import {Http, Response} from 'angular2/http';
import {Observable}     from 'rxjs/Observable';
import {Headers, RequestOptions} from 'angular2/http';
import {AuthHttp, JwtHelper} from 'angular2-jwt';

import {CONFIG} from '../../config';

interface News {
  // accessToken: string;
}


@Injectable()

export class NewsService {

  constructor(public http: Http, public _authHttp: AuthHttp, public _jwt: JwtHelper) {

  }

  private _API_NEWS = `${CONFIG.API_URL}/news`;


  getAllNews(): Observable<News> {

    return this._authHttp.get(this._API_NEWS)
      .map(res => <News>res.json())
      .catch(this.handleError);
  }


  getNews(id: any): Observable<News> {
    return this._authHttp.get(`${this._API_NEWS}/${id}`)
      .map(res => <News>res.json())
      .catch(this.handleError);
  }

  deleteNews(id: any): Observable<News> {
    return this._authHttp.delete(`${this._API_NEWS}/${id}`)
      .map(res => <News>res.json())
      .catch(this.handleError);
  }

  addNews(info: any): Observable<News> {
    let body = JSON.stringify(info);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this._authHttp.post(this._API_NEWS, body, options)
      .map(res => <News>res.json().message)
      .catch(this.handleError);
  }

  editNews(info: any, id: any): Observable<News> {
    let body = JSON.stringify(info);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this._authHttp.put(`${this._API_NEWS}/${id}`, body, options)
      .map(res => <News>res.json().message)
      .catch(this.handleError);
  }

  private handleError(error: Response) {
    // in a real world app, we may send the error to some remote logging infrastructure
    // instead of just logging it to the console
    // console.error(error);
    return Observable.throw(error.json() || 'Server error');
  }

}
