import {Injectable} from 'angular2/core';

declare var swal: any;


@Injectable()

export class SweetAlertService {

  constructor() {

  }

  swal(text: any, type: any, timer: number = 60000, confirm: any = true) {
    let _title;
    switch (type) {
      case 'error':
        _title = 'Lỗi';
        break;
      case 'success':
        _title = 'Thành công';
        break;
      case 'warning':
        _title = 'Chú ý';
        break;
      default:
        _title = 'Thành công';
        break;
    }
    return swal({
      title: _title,
      text: text,
      timer: timer,
      showConfirmButton: confirm,
      type: type,
      html: true,
    });
  }
  swalConfirm(title: any, text: any, abc: any) {
    return swal({
      title: title,
      text: text,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Đồng ý',
      cancelButtonText: 'Không',
      type: 'warning',
      closeOnConfirm: false,
      showCancelButton: true,
      showLoaderOnConfirm: true,
      html: true
    }, () => { abc(true); });
  }

  swalInput(cb: any) {
    return swal({
      title: 'Mật khẩu phòng',
      text: 'Nhập mật khẩu phòng',
      type: 'input',
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
      animation: 'pop',
      inputPlaceholder: '',
      html: true
    }, (inputValue) => {
        if (inputValue === false) return false;
        if (inputValue === '') {
          swal.showInputError('Bạn cần phải nhập mật khẩu!');
          return false;
        } else {
          cb(inputValue);
        }
      });
  }

  close() {
    return swal.close();
  }
}
