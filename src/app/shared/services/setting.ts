import {Injectable} from 'angular2/core';

@Injectable()
export class SettingService {

  constructor() { }

  setBg(game: any = 'none') {
    switch (game) {
      case 'lol':
        (<any>$('body')).css('background-image', 'url("assets/img/game/lol.jpg")');
        break;
      case 'fifa':
        (<any>$('body')).css('background-image', 'url("assets/img/game/fifa.jpg")');
        break;
      case 'tournament':
        (<any>$('body')).css('background-image', 'url("assets/img/game/tournament.jpg")');
        break;
      case 'vltk':
        (<any>$('body')).css('background-image', 'url("assets/img/game/vltk.jpg")');
        break;
      default:
        (<any>$('body')).css('background-image', 'url("assets/img/game/bg-d.jpg")');
    };
  }

}
