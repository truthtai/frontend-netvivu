import {Injectable} from 'angular2/core';
import {Http, Response} from 'angular2/http';
import {Observable}     from 'rxjs/Observable';
import {Headers, RequestOptions} from 'angular2/http';
import {AuthHttp, JwtHelper} from 'angular2-jwt';
import {CONFIG} from '../../config';

declare var window;
interface Order {
  // accessToken: string;
}


@Injectable()

export class OrderService {

  constructor(public http: Http, public _authHttp: AuthHttp, public _jwt: JwtHelper) {

  }

  private _API_ = `${CONFIG.API_URL}/order`;


  getAll(): Observable<Order> {

    return this._authHttp.get(this._API_)
      .map(res => <Order>res.json())
      .catch(this.handleError);
  }

  get(id: any): Observable<Order> {
    return this._authHttp.get(`${this._API_}/${id}`)
      .map(res => <Order>res.json())
      .catch(this.handleError);
  }

  delete(id: any): Observable<Order> {
    return this._authHttp.delete(`${this._API_}/${id}`)
      .map(res => <Order>res.json())
      .catch(this.handleError);
  }

  add(info: any) {
    let body = JSON.stringify(info);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this._authHttp.post(this._API_, body, options)
      .map(res => <Order>res.json().message)
      .catch(this.handleError);
  }

  edit(info: any, id: any): Observable<Order> {
    let body = JSON.stringify(info);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this._authHttp.put(`${this._API_}/${id}`, body, options)
      .map(res => <Order>res.json().message)
      .catch(this.handleError);
  }


  private handleError(error: Response) {
    // in a real world app, we may send the error to some remote logging infrastructure
    // instead of just logging it to the console
    // console.error(error);
    return Observable.throw(error.json() || 'Server error');
  }

}
