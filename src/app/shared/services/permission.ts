import {Injectable} from 'angular2/core';
import {Http, Response} from 'angular2/http';
import {Observable}     from 'rxjs/Observable';
import {Headers, RequestOptions} from 'angular2/http';
import {AuthHttp, JwtHelper} from 'angular2-jwt';
import {CONFIG} from '../../config';

interface Permission {
  // accessToken: string;
}


@Injectable()

export class PermissionService {

  constructor(public http: Http, public _authHttp: AuthHttp, public _jwt: JwtHelper) {

  }

  private _API_PERMISSION = `${CONFIG.API_URL}/permission`;


  getAllPermission(): Observable<Permission> {

    return this._authHttp.get(this._API_PERMISSION)
      .map(res => <Permission>res.json())
      .catch(this.handleError);
  }

  addPermission(info: any): Observable<Permission> {
    let body = JSON.stringify(info);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this._authHttp.post(this._API_PERMISSION, body, options)
      .map(res => <Permission>res.json())
      .catch(this.handleError);
  }

  private handleError(error: Response) {
    // in a real world app, we may send the error to some remote logging infrastructure
    // instead of just logging it to the console
    // console.error(error);
    return Observable.throw(error.json() || 'Server error');
  }

}



