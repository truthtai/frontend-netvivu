import {Injectable} from 'angular2/core';
import {Http, Response} from 'angular2/http';
import {Observable}     from 'rxjs/Observable';
import {Headers, RequestOptions} from 'angular2/http';
import {AuthHttp, JwtHelper} from 'angular2-jwt';
import {CONFIG} from '../../config';

declare const io;
declare const feathers;
interface User {
  // accessToken: string;
}

interface AccessToken {
  accessToken: string;
}

interface UserInfo {

}

@Injectable()
export class UserService {

  constructor(
    public http: Http,
    public _authHttp: AuthHttp,
    public _jwt: JwtHelper
    ) {

  }

  private _API_USER = `${CONFIG.API_URL}/user`;
  private _TOKEN = `${this._API_USER}/me/token`;
  private _loginUrl = `${this._API_USER}/login`;
  private _registerUrl = `${this._API_USER}`;
  private _profile = `${this._API_USER}/me`;
  private _addCreditUrl = `${this._API_USER}/me/credit/add`;
  private _upgradeCPM = `${this._API_USER}/me/upgradeCPM`;
  private _upgradeCTV = `${this._API_USER}/me/upgradeCTV`;
  private _extendCPM = `${this._API_USER}/me/extendCPM`;
  private _addUser = `${this._API_USER}/me/addUser`;
  private _getUserByUser = `${this._API_USER}/me/user`;
  private _historyUser = `${this._API_USER}/me/logs`;
  private _extractCredit = `${this._API_USER}/me/extract-credit`;
  private _REPORT_USER = `${this._API_USER}/report`;
  private _SEARCH_USER = `${this._API_USER}/members`;
  private _JX = `${CONFIG.API_URL}/jx`;

  getToken(): Observable<User> {
    return this._authHttp.get(this._TOKEN)
      .map(res => <User>res.json().accessToken)
      .catch(this.handleError);
  }

  getAllLogs(): Observable<User> {
    return this._authHttp.get(`${CONFIG.API_URL}/logs`)
      .map(res => <User>res.json())
      .catch(this.handleError);
  }

  getLogByUser(user: any): Observable<User> {
    return this._authHttp.get(`${CONFIG.API_URL}/logs/user/${user}`)
      .map(res => <User>res.json())
      .catch(this.handleError);
  }

  searchUser(name: any): Observable<User> {
    return this._authHttp.get(`${this._SEARCH_USER}?search=${name}`)
      .map(res => <User>res.json())
      .catch(this.handleError);
  }

  getReportUser(): Observable<User> {
    return this._authHttp.get(`${this._REPORT_USER}`)
      .map(res => <User>res.json())
      .catch(this.handleError);
  }

  extractCredit(data: any): Observable<User> {
    let body = JSON.stringify(data);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    const API = `${this._extractCredit}/${data.username}`;
    return this._authHttp.put(API, body, options)
      .map(res => <User>res.json())
      .catch(this.handleError);
  }

  getAllUser(value?: any): Observable<User> {
    let getAllUser_API;
    if (value) {
      getAllUser_API = `${this._API_USER}?search=${value}`;
    } else {
      getAllUser_API = this._API_USER;
    }
    return this._authHttp.get(getAllUser_API)
      .map(res => <User>res.json())
      .catch(this.handleError);
  }

  getAllMember(value?: any): Observable<User> {
    let getAllUser_API;
    if (value) {
      getAllUser_API = `${this._API_USER}/me/searchUser?search=${value}`;
    } else {
      getAllUser_API = this._getUserByUser;
    }
    return this._authHttp.get(getAllUser_API)
      .map(res => <User>res.json())
      .catch(this.handleError);
  }

  getAllUserByUser(): Observable<User> {

    return this._authHttp.get(this._getUserByUser)
      .map(res => <User>res.json())
      .catch(this.handleError);
  }

  getProfile(): Observable<User> {

    return this._authHttp.get(this._profile)
      .map(res => <User>res.json())
      .catch(this.handleError);
  }


  extract() {
    let token: any = localStorage.getItem('auth_token');
    if (token) {
      return this._jwt.decodeToken(token);
    } else {
      return false;
    }
  }

  getTypeUser() {
    let token: any = localStorage.getItem('auth_token');
    if (token) {
      return this._jwt.decodeToken(token).type;
    } else {
      return false;
    }
  }

  _add_token_key(res): any {
    localStorage.setItem('auth_token', res);
    return this.extract();
  }


  checkLogin() {
    return new Promise((resolve) => {
      this.extract()
        .subscribe((result) => {
        if (result) {
          resolve(true);
        } else {
          resolve(false);
        }
      });
    });
    //return Observable.of(this.userInfo);
  }

  checkType() {
    // return Observable.of(this.loggedIn);
  }

  login(info: any): Observable<AccessToken> {
    let body = JSON.stringify(info);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this._authHttp.post(this._loginUrl, body, options)
      .map(res => <AccessToken>res.json().accessToken)
      .catch(this.handleError);
  }

  register(info: any): Observable<AccessToken> {
    var regUrl;
    if (this.extract()) {
      regUrl = this._addUser;
    } else {
      regUrl = this._registerUrl;
    }
    let body = JSON.stringify(info);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this._authHttp.post(regUrl, body, options)
      .map(res => <AccessToken>res.json().accessToken)
      .catch(this.handleError);
  }

  editUserID(info: any, id: any) {
    let body = JSON.stringify(info);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this._authHttp.put(`${this._API_USER}/${id}`, body, options)
      .map(res => <AccessToken>res.json())
      .catch(this.handleError);
  }

  addCredit(info: any): Observable<AccessToken> {
    let body = JSON.stringify(info);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this._authHttp.post(this._addCreditUrl, body, options)
      .map(res => <AccessToken>res.json().accessToken)
      .catch(this.handleError);
  }
  upgradeCPM(info: any): Observable<AccessToken> {
    let body = JSON.stringify(info);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this._authHttp.post(this._upgradeCPM, body, options)
      .map(res => <AccessToken>res.json().accessToken)
      .catch(this.handleError);
  }

  upgradeCTV(info?: any): Observable<AccessToken> {
    let body = JSON.stringify(info);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this._authHttp.post(this._upgradeCTV, body, options)
      .map(res => <AccessToken>res.json().accessToken)
      .catch(this.handleError);
  }

  upgradeCPMByCTV(id: any, info: any): Observable<AccessToken> {
    let body = JSON.stringify(info);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this._authHttp.post(`${this._getUserByUser}/${id}/upgradeCPM`, body, options)
      .map(res => <AccessToken>res.json())
      .catch(this.handleError);
  }

  extendCPMByCTV(id: any): Observable<AccessToken> {
    return this._authHttp.get(`${this._getUserByUser}/${id}/extendCPM`)
      .map(res => <AccessToken>res.json().accessToken)
      .catch(this.handleError);
  }

  extendCPM(): Observable<AccessToken> {
    return this._authHttp.get(this._extendCPM)
      .map(res => <AccessToken>res.json().accessToken)
      .catch(this.handleError);
  }

  getHistory(): Observable<UserInfo> {
    return this._authHttp.get(this._historyUser)
      .map(res => <UserInfo>res.json())
      .catch(this.handleError);
  }

  lockUser(id: string): Observable<UserInfo> {
    return this._authHttp.delete(`${this._API_USER}/${id}`)
      .map(res => <UserInfo>res.json())
      .catch(this.handleError);
  }

  deleteUserByOwner(id: string): Observable<UserInfo> {
    return this._authHttp.delete(`${this._getUserByUser}/${id}`)
      .map(res => <UserInfo>res.json())
      .catch(this.handleError);
  }

  joinJx(info: any): Observable<AccessToken> {
    let body = JSON.stringify(info);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this._authHttp.post(this._JX, body, options)
      .map(res => <AccessToken>res.json())
      .catch(this.handleError);
  }

  getJx(): Observable<AccessToken> {
    return this._authHttp.get(this._JX)
      .map(res => <AccessToken>res.json())
      .catch(this.handleError);
  }

  tradeKnbJx(info: any): Observable<AccessToken> {
    let body = JSON.stringify(info);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this._authHttp.post(`${this._JX}/knb`, body, options)
      .map(res => <AccessToken>res.json())
      .catch(this.handleError);
  }

  requestResetPassword(email: any): Observable<AccessToken> {
    return this._authHttp.get(`${this._API_USER}/reset-password?email=${email}`)
      .map(res => <AccessToken>res.json())
      .catch(this.handleError);
  }

  resetPassword(email, code, password): Observable<AccessToken> {
    let body = JSON.stringify({ code, password });
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this._authHttp.post(`${this._API_USER}/reset-password?email=${email}`, body, options)
      .map(res => <AccessToken>res.json())
      .catch(this.handleError);
  }

  private handleError(error: Response) {
    // in a real world app, we may send the error to some remote logging infrastructure
    // instead of just logging it to the console
    // console.error(error);
    return Observable.throw(error.json() || 'Server error');
  }

}
