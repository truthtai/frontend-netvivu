import {Injectable} from 'angular2/core';
import {Http, Response} from 'angular2/http';
import {Observable}     from 'rxjs/Observable';
import {Headers, RequestOptions} from 'angular2/http';
import {AuthHttp, JwtHelper} from 'angular2-jwt';
import {CONFIG} from '../../config';

interface Address {
  // accessToken: string;
}

@Injectable()

export class AddressService {

  constructor(public http: Http, public _authHttp: AuthHttp, public _jwt: JwtHelper) {

  }

  private _API_ADDRESS = `${CONFIG.API_URL}/address`;
  private _city = `${this._API_ADDRESS}/city`;
  private _district = `${this._API_ADDRESS}/city`;

  getAllCity(): Observable<Address> {

    return this._authHttp.get(this._city)
      .map(res => <Address>res.json())
      .catch(this.handleError);
  }

  getAllDistrict(id: any): Observable<Address> {

    return this._authHttp.get(`${this._district}/${id}`)
      .map(res => <Address>res.json().DISTRICTS)
      .catch(this.handleError);
  }

  private handleError(error: Response) {
    // in a real world app, we may send the error to some remote logging infrastructure
    // instead of just logging it to the console
    // console.error(error);
    return Observable.throw(error.json() || 'Server error');
  }

}
