import {Injectable} from 'angular2/core';
import {Http, Response} from 'angular2/http';
import {Observable}     from 'rxjs/Observable';
import {Headers, RequestOptions} from 'angular2/http';
import {AuthHttp, JwtHelper} from 'angular2-jwt';
import {CONFIG} from '../../config';

declare var window;
interface Challenge {
  // accessToken: string;
}


@Injectable()

export class PredictService {

  constructor(public http: Http, public _authHttp: AuthHttp, public _jwt: JwtHelper) {

  }

  private _API_ = `${CONFIG.API_URL}/predict`;


  getAllPredict(sort): Observable<Challenge> {
    return this._authHttp.get(`${this._API_}?sort=${sort}`)
      .map(res => <Challenge>res.json())
      .catch(this.handleError);
  }

  get(id: any): Observable<Challenge> {
    return this._authHttp.get(`${this._API_}/${id}`)
      .map(res => <Challenge>res.json())
      .catch(this.handleError);
  }

  delete(id: any): Observable<Challenge> {
    return this._authHttp.delete(`${this._API_}/${id}`)
      .map(res => <Challenge>res.json())
      .catch(this.handleError);
  }

  add(info: any): Observable<Challenge> {
    let body = JSON.stringify(info);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this._authHttp.post(this._API_, body, options)
      .map(res => <Challenge>res.json().message)
      .catch(this.handleError);
  }

  edit(info: any, id: any): Observable<Challenge> {
    let body = JSON.stringify(info);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this._authHttp.put(`${this._API_}/${id}`, body, options)
      .map(res => <Challenge>res.json().message)
      .catch(this.handleError);
  }

  join(id: any, select: any): Observable<Challenge> {
    let body = JSON.stringify({ select: select });
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this._authHttp.post(`${this._API_}/${id}/join`, body, options)
      .map(res => <Challenge>res.json())
      .catch(this.handleError);
  }

  setWin(id: any, select: any): Observable<Challenge> {
    let body = JSON.stringify({ isWin: select });
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this._authHttp.post(`${this._API_}/${id}/win`, body, options)
      .map(res => <Challenge>res.json().message)
      .catch(this.handleError);
  }

  exportChallenge(id: any) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', `${this._API_}/export/${id}`, true);
    xhr.setRequestHeader('authorization', 'Bearer ' + localStorage.getItem('auth_token'));
    xhr.responseType = 'arraybuffer';

    xhr.onload = function(oEvent) {
      var arrayBuffer = xhr.response; // Note: not oReq.responseText
      if (arrayBuffer) {
        var blob = new Blob([arrayBuffer], { type: 'application/vnd.openxmlformats' });
        var fileName = 'Danh-sach-cac-doi-tham-gia.xlsx';
        var url = window.URL.createObjectURL(blob);
        var a: any = document.createElement('a');
        document.body.appendChild(a);
        a.style = 'display: none';
        a.href = url;
        a.download = fileName;
        a.click();
        window.URL.revokeObjectURL(url);
      }
    };

    xhr.send(null);
  }


  private handleError(error: Response) {
    // in a real world app, we may send the error to some remote logging infrastructure
    // instead of just logging it to the console
    // console.error(error);
    return Observable.throw(error.json() || 'Server error');
  }

}
