import {bind, Component, View, ElementRef, OnInit, EventEmitter, Output, Injectable, ComponentRef} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';
import {Http} from 'angular2/http';

declare var tinymce: any;

@Component({
  selector: 'unity-tinymce',
  moduleId: module.id,
  template: require('./view.html'),
  inputs: ['mceContent'],
  outputs: ['contentChanged']
})

@Injectable()

export class UNITYTinyMCE {

  private elementRef: ElementRef;
  private elementID: string;
  private htmlContent: string;

  @Output() contentChanged: EventEmitter<any>;

  constructor() {

    var randLetter = String.fromCharCode(65 + Math.floor(Math.random() * 26));
    var uniqid = randLetter + Date.now();

    this.elementID = 'tinymce' + uniqid;
    this.contentChanged = new EventEmitter();
  }

  ngAfterViewInit() {
    //Clone base textarea
    var baseTextArea = this.elementRef.nativeElement.querySelector("#baseTextArea");
    var clonedTextArea = baseTextArea.cloneNode(true);
    clonedTextArea.id = this.elementID;

    var formGroup = this.elementRef.nativeElement.querySelector("#tinyFormGroup");
    formGroup.appendChild(clonedTextArea);

    //Attach tinyMCE to cloned textarea
    tinymce.init(
      {
        mode: 'exact',
        height: 500,
        theme: 'modern',
        plugins: [
          'advlist autolink lists link image charmap print preview anchor',
          'searchreplace visualblocks code fullscreen',
          'insertdatetime media table contextmenu paste code'
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        elements: this.elementID,
        setup: this.tinyMCESetup.bind(this)
      }
    );
  }

  ngOnDestroy() {
    //destroy cloned elements
    tinymce.get(this.elementID).remove();

    var elem = document.getElementById(this.elementID);
    elem.parentElement.removeChild(elem);
  }


  tinyMCESetup(ed) {
    ed.on('keyup', this.tinyMCEOnKeyup.bind(this));
  }

  tinyMCEOnKeyup(e) {
    this.contentChanged.emit(tinymce.get(this.elementID).getContent());
  }


  set mceContent(content) {
    this.htmlContent = content;
  }
}
