//our root app component
import {Component, Directive, ElementRef, NgZone, EventEmitter, Input, Output} from 'angular2/core';
import {bootstrap} from 'angular2/platform/browser';
declare var CKEDITOR: any;
@Directive({
  selector : '[my-ckeditor]'
})
export class CKEditor {
  @Input() ngModel;
  @Output() ngModelChange = new EventEmitter(false);
  constructor(eRef: ElementRef, zone: NgZone) {
    let editor = CKEDITOR.replace(eRef.nativeElement);
    CKEDITOR.config.width = '100%';
    editor.on('change', (evt) => {
        //console.log(evt.editor.getData());
        zone.run(() =>  this.ngModelChange.emit(evt.editor.editable().getText()));
    });
  }
}
