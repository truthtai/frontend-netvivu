import {Component, Input, ElementRef} from 'angular2/core';
import {UserService} from '../../shared/services/user';
import {Observable} from 'rxjs/Rx';
import {Router} from 'angular2/router';
import {RouteParams} from 'angular2/router';
import {ChallengeService} from '../../shared/services/challenge';
import { SettingService } from '../../shared/services/setting';
import {JwtHelper} from 'angular2-jwt';
import {DateFormatPipe} from 'angular2-moment';
import * as _ from 'lodash';
import * as moment from 'moment';
import { SweetAlertService } from '../../shared/services/sweet-alert';
import {SocketService} from '../../shared/services/socket';
declare var toastr: any;

@Component({
  selector: 'challenge-detail',
  template: require('./detail.html'),
  styles: [require('./detail.scss')],
  providers: [UserService, ChallengeService, JwtHelper, SettingService, SweetAlertService, SocketService],
  directives: [],
  pipes: [DateFormatPipe]
})

export class ChallengeDetail {
  challenge: any = {};
  currentTime: any = moment().unix();
  userData: any;
  isAuthenticated: any = false;
  startTimeMin: any = 0;
  startTimeSec: any = 0;
  startTimeHour: any = 0;
  startTimeSecz: number = 0;
  isTeam: any;
  countTimer = {
    sec: 0,
    min: 0
  };
  // config = {
  //   countTime: {},
  //   teamA: {
  //     locked: false,
  //     started: false,
  //     isWin: null
  //   },
  //   teamB: {
  //     locked: false,
  //     started: false,
  //     isWin: null
  //   }
  // };
  constructor(
    private _userService: UserService,
    public _CS: ChallengeService,
    public _setting: SettingService,
    public router: Router,
    public _e: ElementRef,
    public _RP: RouteParams,
    public _notif: SweetAlertService,
    public _SS: SocketService
    ) {




    // this.userData = this._userService.extract();
    // this._setting.setBg('tournament');
  }

  ngOnInit() {
    this.join();
  }

  join() {
    this._notif.swalInput((value) => {
      this._CS.join(this._RP.get('id'), value).subscribe(
        res => {
          this.gameInit(res);
          this._notif.close();
        }
        , (error: any) => {
          toastr.options.positionClass = 'toast-bottom-right';
          toastr.error(error.message);
          this.join();
        }
        );
    });
  }

  gameInit(res) {
    this.getChallenge(res);
    this.getTeam();
    this.getJoin();
    this.getMsg();
    this.getConfig();
    this.getCountTimer();
    this.getIsWin();
    this.startTimeHour = this.pad(this.startTimeHour);
    this.startTimeMin = this.pad(this.startTimeMin);
    this.startTimeSec = this.pad(this.startTimeSec);

  }

  getJoin() {
    this._SS.joinRoom(this.challenge).subscribe(
      res => {
        toastr.options.positionClass = 'toast-bottom-right';
        toastr.warning(res);
      }
      , err => console.log(err)
      );
  }

  getMsg() {
    this._SS.getMsg().subscribe(
      res => {
        this._notif.swal('Thông báo: ' + res, 'warning');
      }
      , err => console.log(err)
      );
  }

  getIsWin() {
    this._SS.getIsWin().subscribe(
      res => {
        this.challenge.isWin = res;
      }
      , err => console.log(err)
      );
  }

  getTeam() {
    this._SS.getTeam(this.challenge).subscribe(
      res => {
        this.isTeam = res;
      }
      , err => console.log(err)
      );
  }

  getCountTimer() {
    this._SS.getCountTimer().subscribe(
      res => {
        this.countTimer = res;
      }
      , err => console.log(err)
      );
  }

  getConfig() {
    this._SS.getConfig(this.challenge).subscribe(
      res => {
        this.challenge = res;
      }
      , err => console.log(err)
      );
  }

  getChallenge(res) {
    this.isAuthenticated = true;
    this.challenge = res;
  }

  // startTime() {
  //   let timer = Observable.timer(2000, 1000);
  //   timer.subscribe(t => this.countTimer(t));
  // }
  //
  // countTimer(t) {
  //   this.startTimeHour = this.pad(Math.floor(t / 3600));
  //   this.startTimeMin = this.pad(Math.floor((t - parseInt(this.startTimeHour, 10) * 3600) / 60));
  //   this.startTimeSec = this.pad(t - (parseInt(this.startTimeHour, 10) * 3600 + parseInt(this.startTimeMin, 10) * 60));
  // }

  pad(val) {
    return val > 9 ? val : '0' + val;
  }

  lock() {
    this._SS.confirm(this.challenge);
    setTimeout(() => {
      this._SS.setConfig(this.challenge, { locked: true });
    }, 100);
  }

  cancel() {
    if (!this.challenge.countingTime) {
      this._SS.setConfig(this.challenge, { locked: false });
      this._SS.setConfig(this.challenge, { started: false });
    }
  }

  start() {
    this._SS.setConfig(this.challenge, { started: true });
  }

  doWin() {
    this._SS.setConfig(this.challenge, { isWin: true });
  }

  doLose() {
    this._SS.setConfig(this.challenge, { isWin: false });
  }

  reset() {
    // this._SS.setConfig(this.challenge, { isWin: null });
  }

}
