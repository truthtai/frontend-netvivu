import {Component, Input, ElementRef} from 'angular2/core';
import {UserService} from '../../shared/services/user';
import {Router, RouteParams} from 'angular2/router';
import {ChallengeService} from '../../shared/services/challenge';
import { SettingService } from '../../shared/services/setting';
import {JwtHelper} from 'angular2-jwt';
import {DateFormatPipe} from 'angular2-moment';
import * as _ from 'lodash';
import * as moment from 'moment';
import { SweetAlertService } from '../../shared/services/sweet-alert';

@Component({
  selector: 'challenge-find',
  template: require('./find.html'),
  styles: [require('./find.scss')],
  providers: [UserService, ChallengeService, JwtHelper, SettingService, SweetAlertService],
  directives: [],
  pipes: [DateFormatPipe]
})

export class ChallengeFind {
  challenges: any;
  currentTime: any = moment().unix();
  userData: any;

  constructor(
    private _userService: UserService,
    public _CS: ChallengeService,
    public _setting: SettingService,
    public router: Router,
    public _e: ElementRef,
    public _notif: SweetAlertService
    ) {
    this._CS.getAll().subscribe(
      res => this.challenges = res
      , err => console.log(err)
      );
    this.userData = this._userService.extract();
    this._setting.setBg('tournament');
  }

  join(id: any) {
    // this._notif.swalInput((value) => {
    //   this._CS.join(id, value).subscribe(
    //     res => {
    //       this.router.navigate(['ChallengeDetail', {id: id}]);
    //       this._notif.close();
    //     }
    //     , (error: any) => this._notif.swal('Có lỗi xảy ra ' + error.message, 'error')
    //   );
    // });
    this.router.navigate(['ChallengeDetail', {id: id}]);
  }
  // doDetail(id: string) {
  //   this.router.navigate(['TournamentDetail', {id: id}]);
  // }

}
