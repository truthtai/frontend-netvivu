import {Component, Input, ElementRef} from 'angular2/core';
import {UserService} from '../../shared/services/user';
import {FormBuilder, Validators} from 'angular2/common';
import {Router} from 'angular2/router';
import {ChallengeService} from '../../shared/services/challenge';
import { SettingService } from '../../shared/services/setting';
import {JwtHelper} from 'angular2-jwt';
import {DateFormatPipe} from 'angular2-moment';
import * as _ from 'lodash';
import * as moment from 'moment';
import { SweetAlertService } from '../../shared/services/sweet-alert';

@Component({
  selector: 'challenge-create-room',
  template: require('./create.html'),
  styles: [require('./create.scss')],
  providers: [UserService, ChallengeService, JwtHelper, SettingService, SweetAlertService],
  directives: [],
  pipes: [DateFormatPipe]
})

export class ChallengeCreateRoom {
  challenges: any;
  currentTime: any = moment().unix();
  userData: any;
  challengeCreateRoomForm: any;
  fee: any = 0;
  credit: any = 0;
  submitting: any = false;
  constructor(
    private _userService: UserService,
    public _CS: ChallengeService,
    public _setting: SettingService,
    public router: Router,
    private _fb: FormBuilder,
    public _notif: SweetAlertService,
    public _e: ElementRef
    ) {

    this.challengeCreateRoomForm = this._fb.group({
      'name': ['', Validators.required],
      'inGame1': ['', Validators.required],
      'inGame2': ['', Validators.required],
      'inGame3': ['', Validators.required],
      'inGame4': ['', Validators.required],
      'inGame5': ['', Validators.required],
      'phone': ['', Validators.required],
      'password': ['', Validators.required]
    });

    this._CS.getAll().subscribe(
      (res: any) => {
        this.challenges = _.map(res, (r: any) => {
          r.endRegTs = moment(r.endReg).unix();
          r.startRegTs = moment(r.startReg).unix();
          return r;
        });
      }
      , err => console.log(err)
      );
    this.userData = this._userService.extract();
    this._setting.setBg('tournament');
  }

  doAdd() {
    if (this.challengeCreateRoomForm.dirty && this.challengeCreateRoomForm.valid) {
      this.challengeCreateRoomForm.value.teams = [
        { name: this.challengeCreateRoomForm.value.inGame1 },
        { name: this.challengeCreateRoomForm.value.inGame2 },
        { name: this.challengeCreateRoomForm.value.inGame3 },
        { name: this.challengeCreateRoomForm.value.inGame4 },
        { name: this.challengeCreateRoomForm.value.inGame5 }
      ];
      this.challengeCreateRoomForm.value.credit = this.credit;
      if (this.credit > 0) {
        this.challengeCreateRoomForm.value.fee = this.fee;
        this.submitting = true;
        this._CS.addChallenge(this.challengeCreateRoomForm.value).subscribe(
          (res: any) => {
            this.router.navigate(['ChallengeDetail', { id: res._id }]);
          }
          , err => {
            this._notif.swal('Có lỗi xảy ra ' + err.message, 'error');
            this.submitting = false;
          }
          );
      } else {
        this._notif.swal('Có lỗi xảy ra: Vui lòng chọn 1 phần thưởng ', 'error');
      }
    }

  }

  _change(credit: any) {
    this.fee = credit / 25;
    this.credit = credit;
  }

}
