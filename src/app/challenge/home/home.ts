import {Component, Input, ElementRef} from 'angular2/core';
import {UserService} from '../../shared/services/user';
import {Router} from 'angular2/router';
import {ChallengeService} from '../../shared/services/challenge';
import {SocketService} from '../../shared/services/socket';
import {PredictService} from '../../shared/services/predict';
import { SettingService } from '../../shared/services/setting';
import {JwtHelper} from 'angular2-jwt';
import {DateFormatPipe} from 'angular2-moment';
import * as _ from 'lodash';
import * as moment from 'moment';
import { SweetAlertService } from '../../shared/services/sweet-alert';

@Component({
  selector: 'challenge-home',
  template: require('./home.html'),
  styles: [require('./home.scss')],
  providers: [UserService, ChallengeService, SweetAlertService,
    JwtHelper, SettingService, PredictService, SocketService],
  directives: [],
  pipes: [DateFormatPipe]
})

export class ChallengeHome {
  predictsNow: any = [];
  predictsTimeout: any = [];
  predictsHistory: any = [];
  currentTime: any = moment().unix();
  userData: any;
  constructor(
    private _userService: UserService,
    public _CS: ChallengeService,
    public _PS: PredictService,
    public _setting: SettingService,
    public router: Router,
    public _e: ElementRef,
    public _notif: SweetAlertService,
    public _SS: SocketService
    ) {

    this.userData = this._userService.extract();
    this._setting.setBg('tournament');
    this._SS.getPredictNow().subscribe(
      res => {
        this.predictsNow = res;
      }
      , err => console.log(err)
      );

    this._SS.getPredictHistory().subscribe(
      res => {
        this.predictsHistory = res;
      }
      , err => console.log(err)
      );

    this._SS.getPredictTimeout().subscribe(
      res => {
        this.predictsTimeout = res;
      }
      , err => console.log(err)
      );

    this._PS.getAllPredict('now').subscribe(
      (res: any) => {
        this.predictsNow = res;
      }
      , err => console.log(err)
      );

    this._PS.getAllPredict('timeout').subscribe(
      (res: any) => {
        this.predictsTimeout = res;
      }
      , err => console.log(err)
      );

    this._PS.getAllPredict('history').subscribe(
      (res: any) => {
        this.predictsHistory = res;
      }
      , err => console.log(err)
      );
  }

  join(predict: any, select: any) {
    if (predict.credit > this.userData.credit) {
      this._notif.swal('Không đủ xu để tham gia dự đoán', 'error');
    } else {
      let colorTeam;
      if (select === 'teamA') {
        colorTeam = 'blue';
      } else {
        colorTeam = 'red';
      }
      this._notif.swalConfirm('Tham gia dự đoán', `<p>Trận đấu: <strong>${predict.name}</strong></p>
      <p> Đội thắng là: <strong style="color: ${colorTeam}">${predict.teams[select]}</strong> </p>
      <p> Phí tham gia: <strong>${predict.credit} xu</strong></p> `, (result) => {
          if (result) {
            this._PS.join(predict._id, select).subscribe(
              res => {
                this._notif.swal('Tham gia dự đoán thành công', 'success');
              }
              , err => {
                this._notif.swal('Có lỗi xảy ra: ' + err.message, 'error');
              });
          }
        });
    }

  }

  // doDetail(id: string) {
  //   this.router.navigate(['TournamentDetail', {id: id}]);
  // }

}
