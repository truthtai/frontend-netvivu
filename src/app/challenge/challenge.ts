import {Component} from 'angular2/core';
import {JwtHelper} from 'angular2-jwt';
import {RouteConfig, Router, ROUTER_DIRECTIVES} from 'angular2/router';
import {ChallengeHome} from './home/home';
import {NavHeader} from '../navHeader/navHeader';
import {ChallengeCreateRoom} from './create/create';
import {ChallengeFind} from './find/find';
import {ChallengeDetail} from './detail/detail';

@Component({
  selector: 'challenge',
  template: require('./challenge.html'),
  directives: [NavHeader],
  providers: [JwtHelper]
})

@RouteConfig([
  { path: '/', component: ChallengeHome, as: 'ChallengeHome', useAsDefault: true },
  // { path: '/:id', component: ChallengeDetail, as: 'ChallengeDetail' },
  { path: '/tao-phong', component: ChallengeCreateRoom, as: 'ChallengeCreateRoom' },
  { path: '/tim-phong', component: ChallengeFind, as: 'ChallengeFind' },
  { path: '/phong/:id', component: ChallengeDetail, as: 'ChallengeDetail' },
])

export class Challenge {
  constructor() { }

}
