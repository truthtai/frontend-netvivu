import {Component} from 'angular2/core';
import {NavHeader} from '../navHeader/navHeader';
import {SliderComponent} from '../slider/slider';
import {TournamentService} from '../shared/services/tournament';
import {NewsService} from '../shared/services/news';
import {JwtHelper} from 'angular2-jwt';
import {CONFIG} from '../config';
import {DateFormatPipe} from 'angular2-moment';

@Component({
  selector: 'home',
  template: require('./home.html'),
  styles: [require('./home.scss')],
  directives: [NavHeader, SliderComponent],
  providers: [TournamentService, NewsService, JwtHelper],
  pipes: [DateFormatPipe]
})

export class Home {
  tournaments: any;
  news: any;
  constructor(public _ts: TournamentService, public _ns: NewsService) {
    this._ts.getAllTournament().subscribe(
      (res: any) => this.tournaments = res.map((r) => {
        r.image = `${CONFIG.API_URL}/image/${r.image}`;
        return r;
      })
      , err => console.log(err)
      );
    this._ns.getAllNews().subscribe(
      (res: any) => this.news = res.map((r) => {
        r.image = (<any>$(r.description)).find('img')[0].src;
        r.description = (<any>$(r.description)).text().substring(0, 200) + '...';
        return r;
      })
      , err => console.log(err)
      );

  }

  ngOnInit() {
    // console.log('hello `header` component');
  }

}
