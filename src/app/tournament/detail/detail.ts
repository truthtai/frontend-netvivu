import {Component, Input} from 'angular2/core';
import {RouteParams} from 'angular2/router';
import {TournamentService} from '../../shared/services/tournament';
import {JwtHelper} from 'angular2-jwt';
import {DateFormatPipe} from 'angular2-moment';
import {FormBuilder, Validators} from 'angular2/common';
import { SweetAlertService } from '../../shared/services/sweet-alert';
import { SettingService } from '../../shared/services/setting';
import {CONFIG} from '../../config';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'tournament-detail',
  template: require('./detail.html'),
  styles: [require('./detail.scss')],
  providers: [TournamentService, JwtHelper, SweetAlertService, SettingService],
  directives: [],
  pipes: [DateFormatPipe]
})

export class TournamentDetail {
  tournament: any;
  currentTime: any = moment().unix();
  registerForm: any;
  constructor(
    public _tournamentService: TournamentService,
    public _params: RouteParams,
    public _fb: FormBuilder,
    public _notif: SweetAlertService,
    public _setting: SettingService
    ) {

    this.registerForm = this._fb.group({
      'phone': ['', Validators.required],
      'name': ['', Validators.required],
    });

    this._tournamentService.getTournament(this._params.get('id')).subscribe(
      (r: any) => {
        r.endRegTs = moment(r.endReg).unix();
        r.startRegTs = moment(r.startReg).unix();
        r.image = `${CONFIG.API_URL}/image/${r.image}`;
        this._setting.setBg(r.game);
        this.tournament = r;

      }
      , err => console.log(err)
      );
  }

  doRegister() {

    if (this.registerForm.dirty && this.registerForm.valid) {
      (<any>$('#joinTournament')).modal('hide');
      (<any>$('#loadingPopup')).modal('show');
      this._tournamentService.join(this.registerForm.value, this.tournament._id)
        .subscribe(
        res => {
          (<any>$('#loadingPopup')).modal('hide');
          this._notif.swal('Bạn đã đăng ký thành công. Chúc bạn một ngày vui vẻ!', 'success');
        },
        error => { this._notif.swal(`Lỗi: ${error.message}`, 'error'); (<any>$('#loadingPopup')).modal('hide'); });
    }
  }

}
