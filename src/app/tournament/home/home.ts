import {Component, Input, ElementRef} from 'angular2/core';
import {UserService} from '../../shared/services/user';
import {Router} from 'angular2/router';
import {TournamentService} from '../../shared/services/tournament';
import { SettingService } from '../../shared/services/setting';
import {JwtHelper} from 'angular2-jwt';
import {DateFormatPipe} from 'angular2-moment';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'tournament-home',
  template: require('./home.html'),
  styles: [require('./home.scss')],
  providers: [UserService, TournamentService, JwtHelper, SettingService],
  directives: [],
  pipes: [DateFormatPipe]
})

export class TournamentHome {
  tournaments: any;
  currentTime: any = moment().unix();
  userData: any;
  constructor(
    private _userService: UserService,
    public _tournamentService: TournamentService,
    public _setting:  SettingService,
    public router: Router,
    public _e: ElementRef
    ) {
    this._tournamentService.getAllTournament().subscribe(
      (res: any) => {
        this.tournaments = _.map(res, (r: any) => {
          r.endRegTs = moment(r.endReg).unix();
          r.startRegTs = moment(r.startReg).unix();
          return r;
        });
      }
      , err => console.log(err)
      );
    this.userData = this._userService.extract();
    this._setting.setBg('tournament');
  }

  doDetail(id: string) {
    this.router.navigate(['TournamentDetail', {id: id}]);
  }

}
