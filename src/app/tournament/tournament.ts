import {Component} from 'angular2/core';
import {JwtHelper} from 'angular2-jwt';
import {RouteConfig, Router, ROUTER_DIRECTIVES} from 'angular2/router';
import {TournamentHome} from './home/home';
import {NavHeader} from '../navHeader/navHeader';
import {TournamentDetail} from './detail/detail';

@Component({
  selector: 'tournament',
  template: require('./tournament.html'),
  directives: [NavHeader],
  providers: [JwtHelper]
})

@RouteConfig([
  { path: '/', component: TournamentHome, as: 'TournamentHome', useAsDefault: true },
  { path: '/:id', component: TournamentDetail, as: 'TournamentDetail' },
])

export class Tournament {
  constructor() { }

}
