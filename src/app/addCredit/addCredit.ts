import {Component} from 'angular2/core';
import {CanActivate, Router} from 'angular2/router';
import {UserService} from '../shared/services/user';
import {JwtHelper} from 'angular2-jwt';
import {NgClass, NgIf, FormBuilder, Validators} from 'angular2/common';
import { ValidationService} from '../shared/services/validation';
import * as _ from 'lodash';
import { SweetAlertService } from '../shared/services/sweet-alert';
import {NavHeader} from '../navHeader/navHeader';

@Component({
  selector: 'add-credit-user',
  template: require('./addCredit.html'),
  styles: [require('./addCredit.scss')],
  providers: [UserService, SweetAlertService, JwtHelper],
  directives: [NgClass, NgIf, NavHeader]
})

// @CanActivate(() => { return UserService.checkLogin() })
// @RouteConfig([
//   { path: '/admin', component: Home, as: 'Home' }
// ])


export class AddCredit {
  addCreditForm: any;
  constructor(
    // public _US: UserService,
    public _UserService: UserService,
    private _fb: FormBuilder,
    public _notif: SweetAlertService,
    public router: Router
    ) {

    this.addCreditForm = this._fb.group({
      'issuer': ['', Validators.required],
      'cardSerial': ['', Validators.required],
      'cardCode': ['', Validators.required]
    });
  }

  doAddCredit() {
    if (!this.addCreditForm.dirty && !this.addCreditForm.valid) {
      this._notif.swal('Xin vui lòng điền đầy đủ thông tin!', 'error');
    } else {
      this._UserService.addCredit(this.addCreditForm.value).subscribe(res => {
        this._UserService._add_token_key(res);
        this._notif.swal('Bạn đã nạp card thành công. XU sẽ được cập nhật ngay lập tức. Hệ thống sẽ tự động chuyển trang', 'success', 5000, false);
        setTimeout(() => { location.reload(); }, 5000);
      }, (error) => { this._notif.swal(`Có lỗi xảy ra: ${error.message}`, 'error'); });
    };
  }

}
