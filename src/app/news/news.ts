import {Component} from 'angular2/core';
import {NavHeader} from '../navHeader/navHeader';
import {SliderComponent} from '../slider/slider';
import {NewsService} from '../shared/services/news';
import {RouteParams} from 'angular2/router';
import {JwtHelper} from 'angular2-jwt';

@Component({
  selector: 'tin-tuc',
  template: require('./news.html'),
  styles: [require('./news.scss')],
  directives: [NavHeader, SliderComponent],
  providers: [NewsService, JwtHelper]
})

export class News {
  news: any;
  constructor(public _ns: NewsService, private _r: RouteParams) {
    console.log(this._r.get('id'));
    this._ns.getNews(this._r.get('id')).subscribe(res => this.news = res, err => console.log(err));
  }

  ngOnInit() {
    // console.log('hello `header` component');
  }

}
